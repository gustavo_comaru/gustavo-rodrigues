import java.util.*;

public class ElfoDaLuz extends Elfo{

    private int qtdAtaques = 0;
    private static final double QTD_CURA = 10.0;
    private static final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<String>();
    
    ElfoDaLuz(String nome){
        super(nome);
        QTD_DANO = 21.0;
        DESCRICOES_OBRIGATORIAS.add("Espada de Galvorn");
        ganharItem(new ItemImperdivel("Espada de Galvorn", 1));
    }
    
    @Override
    public void perderItem(Item itemPerdido){
        boolean podePerder = !DESCRICOES_OBRIGATORIAS.contains(itemPerdido.getDescricao());
        if(podePerder) super.perderItem(itemPerdido);
    }
    
    public void atacarComEspada(Dwarf alvo){
        if(status == Status.MORTO) return;
        alvo.sofrerDano();
        qtdAtaques++;
        if(devePerderVida()) sofrerDano();
        else receberCura();
    }
    
    private boolean devePerderVida(){
        return qtdAtaques % 2 == 1;
    }
    
    private void receberCura(){
       vida += QTD_CURA;
    }
    
}
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.service.ValorMidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/valormidia")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class ValorMidiaRestController extends AbstractCrudRestController<ValorMidia, Long, ValorMidiaService> {
    
    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected ValorMidiaService getService() {
        return valorMidiaService;
    }
    
    @GetMapping("/va")
    public ResponseEntity va(){
        return ResponseEntity.ok( valorMidiaService.findValorAtual(3L) );
    }
    
}
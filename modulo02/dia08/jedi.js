class Jedi{

 constructor(nome){

    this.nome = nome

    this.h1 = document.createElement("h1")
    this.h1.innerText = this.nome
    this.h1.id = `jedi_${ this.nome }`
    const dadosJedi = document.getElementById("dadosJedi")
    dadosJedi.appendChild(this.h1)

  }
  
  atacarComSelf(){
    let self = this
    setTimeout( function(){
      self.h1.innerText += " atacou!"
    }, 1000 ) 
  }

  atacarComBind(){
    setTimeout( function(){
      this.h1.innerText += " atacou!"
    }.bind(this), 1000 ) 
  }

  atacarComArrow(){
    setTimeout( () => {
      this.h1.innerText += " atacou!"
    }, 1000 ) 
  }

}

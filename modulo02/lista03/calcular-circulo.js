function calcularCirculo(circ){

  if(! typeof(circ["raio"]) === "number")
    return undefined

  if(! typeof(circ["tipoCalculo"]) === "string")
    return undefined

  if(circ["tipoCalculo"] === "A")
    return Math.PI * circ["raio"] * circ["raio"]

  if(circ["tipoCalculo"] === "C")
    return 2 * Math.PI * circ["raio"]

  return undefined
  
}

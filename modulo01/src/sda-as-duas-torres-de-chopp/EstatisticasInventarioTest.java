import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class EstatisticasInventarioTest{
    @Test
    public void calcularMediaInventarioVazio(){
        assertEquals(Double.NaN, new EstatisticasInventario(new Inventario()).calcularMedia(), 0.1);
    }
    @Test
    public void calcularMediaResultadoInteiro(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Arvore", 1));
        inventario.adicionar(new Item("Arbusto", 3));
        inventario.adicionar(new Item("Flor", 5));
        assertEquals(3.0, new EstatisticasInventario(inventario).calcularMedia(), 0.1);
    }
    @Test
    public void calcularMediaResultadoRacional(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Arvore", 3));
        inventario.adicionar(new Item("Arbusto", 4));
        inventario.adicionar(new Item("Flor", 6));
        inventario.adicionar(new Item("Fabrica de Pneus", 1));  
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(3.5, resultado, 0.1);        
    }
    @Test
    public void calcularMedianaInventarioVazio(){
        assertEquals(Double.NaN, new EstatisticasInventario(new Inventario()).calcularMediana(), 0.1);    
    }
    @Test
    public void calcularMedianaInventarioComNumeroDeItensImpar(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Arvore", 5));
        inventario.adicionar(new Item("Arbusto", 3));
        inventario.adicionar(new Item("Flor", 4));
        double resultadoObtido = new EstatisticasInventario(inventario).calcularMediana();
        assertEquals(4, resultadoObtido, 0.1);
    }
    @Test
    public void calcularMediaInventarioComNumeroDeItensPar(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Arvore", 7));
        inventario.adicionar(new Item("Arbusto", 3));
        inventario.adicionar(new Item("Flor", 2));
        inventario.adicionar(new Item("Fabrica de Pneus", 4));
        double resultadoObtido = new EstatisticasInventario(inventario).calcularMediana();
        assertEquals(3.5, resultadoObtido, 0.1);
    }
    @Test
    public void calcularQtdItensAcimaDaMediaInventarioVazio(){
        assertEquals(0, new EstatisticasInventario(new Inventario()).qtdItensAcimaDaMedia());
    }
    @Test
    public void calcularQtdItensAcimaDaMediaInventarioComItens(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Arvore", 3));
        inventario.adicionar(new Item("Arbusto", 4));
        inventario.adicionar(new Item("Flor", 6));
        inventario.adicionar(new Item("Fabrica de Pneus", 1));  
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(2, estatisticas.qtdItensAcimaDaMedia());
    }
}
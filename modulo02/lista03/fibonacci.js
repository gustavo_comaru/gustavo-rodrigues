function fibo(num){

  if(! (Number.isInteger(num) && num > 0))
    return undefined

  if(num === 1 ||  num === 2)
    return 1

  return fibo(num-2) + fibo(num-1)

}

function fiboSum(num){

  if(! (Number.isInteger(num) && num > 0))
    return undefined

  var soma = 0

  while(num > 0){
    soma += fibo(num)
    num--
  }

  return soma

}

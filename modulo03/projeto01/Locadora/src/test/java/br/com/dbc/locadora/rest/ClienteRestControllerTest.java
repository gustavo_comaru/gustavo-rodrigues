package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.repository.ClienteRepository;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class ClienteRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private ClienteRestController clienteRestController;

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    protected AbstractCrudRestController getController() {
        return clienteRestController;
    }

    @Before
    public void beforeTest() {
        clienteRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    @Test
    @Ignore
    @WithMockUser(username = "admin.admin", password = "jwtpass", authorities = {"ADMIN_USER"} )
    public void clienteCreateSuccessTest() throws Exception {
        
        Cliente c = Cliente.builder()
            .nome("nome")
            //.endereco("endereco")
            .telefone(99999L)
            .build();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()));
                //.andExpect(MockMvcResultMatchers.jsonPath("$.endereco").value(c.getEndereco()));
        
        List<Cliente> clientes = clienteRepository.findAll();
        
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(c.getNome(), clientes.get(0).getNome());
        //Assert.assertEquals(c.getEndereco(), clientes.get(0).getEndereco());
        Assert.assertEquals(c.getTelefone(), clientes.get(0).getTelefone());
    
    }
    
    @Test
    @Ignore
    public void clienteUpdateSuccessTest() throws Exception {
    
        Cliente cliente = Cliente.builder()
            .nome("nome")
            //.endereco("endereco")
            .telefone(12345678L)
            .build();
    
        Cliente clienteSalvo = clienteRepository.save(cliente);
        
        Cliente cliente2 = Cliente.builder()
            .id(clienteSalvo.getId())
            .nome("nome2")
            //.endereco("endereco2")
            .telefone(87654321L)
            .build();
        
        restMockMvc.perform(MockMvcRequestBuilders.put("/api/cliente/{id}", cliente2.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(cliente2)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(cliente2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(cliente2.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(cliente2.getTelefone()));
                //.andExpect(MockMvcResultMatchers.jsonPath("$.endereco").value(cliente2.getEndereco()));
        
        Cliente clienteAchado = clienteRepository.findById( cliente2.getId() ).get();
        
        Assert.assertEquals(cliente2.getNome(), clienteAchado.getNome());
        //Assert.assertEquals(cliente2.getEndereco(), clienteAchado.getEndereco());
        Assert.assertEquals(cliente2.getTelefone(), clienteAchado.getTelefone());
        
    }
    
    @Test
    @Ignore
    public void clienteUpdateFindByIdSuccess() throws Exception {
        
        Cliente clienteBuildado = Cliente.builder()
        .nome("fulano")
        //.endereco("rua central")
        .telefone(12432L)
        .build();
    
        Cliente clienteSalvo = clienteRepository.save( clienteBuildado );
        
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente/{id}", clienteSalvo.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(clienteSalvo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(clienteSalvo.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(clienteSalvo.getTelefone()));
          //      .andExpect(MockMvcResultMatchers.jsonPath("$.endereco").value(clienteSalvo.getEndereco()));
        
        //Cliente clienteEncontrado = clienteRepository
    
    }

}

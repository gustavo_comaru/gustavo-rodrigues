import java.util.ArrayList;

public class EstatisticasInventario{

    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public double calcularMedia(){
        if(inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        double somaDeQtd = 0;
        int contadorDeQtd = 0;
        for(int i = 0; i < inventario.size(); i++){
            if(inventario.obter(i) != null){
                contadorDeQtd++;
                somaDeQtd += inventario.obter(i).getQuantidade();
            }
        }
        return somaDeQtd / contadorDeQtd * 1.0;
    }
    
    public double calcularMediana(){
        if(inventario.getItens().isEmpty()){
            return Double.NaN;
        }        
        Inventario inventarioOrdenado = inventario;
        inventarioOrdenado.ordenarItens();
        int qtdItens = inventarioOrdenado.quantidadeDeItens();
        if(qtdItens % 2 == 1){
            return inventarioOrdenado.obter(qtdItens / 2).getQuantidade();
        }
        return (inventarioOrdenado.obter(qtdItens/2).getQuantidade() + inventarioOrdenado.obter(qtdItens/2-1).getQuantidade()) / 2.0;
    }
    
    public int qtdItensAcimaDaMedia(){
        if(inventario.getItens().isEmpty()){
            return 0;
        }
        double media = calcularMedia();
        int contadorQtdAcimaDaMedia = 0;        
        for(int i = 0; i < inventario.size(); i++){
            if(inventario.obter(i) != null && inventario.obter(i).getQuantidade() > media){
                contadorQtdAcimaDaMedia++;
            }
        }
        return contadorQtdAcimaDaMedia;
    }
    
}
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.ClienteDAO;
import br.com.dbc.petshophibernate.entity.Animal;
import br.com.dbc.petshophibernate.entity.Cliente;
import java.util.Arrays;

public class ClienteService extends AbstractCrudService<Cliente, Long, ClienteDAO> {

    private static ClienteService instance;
    
    static{
        instance = new ClienteService();
    }
    
    public static ClienteService getInstance(){
        return instance;
    }
    
    @Override
    protected ClienteDAO getDAO() {
        return ClienteDAO.getInstance();
    }
    
    public void criarDezClientesComDezAnimaisCada(){
        Animal[] animais = new Animal[10];
        int contadorAnimal, contadorCliente = 0;
        while( contadorCliente < 10 ){
            contadorAnimal = 0;
            while( contadorAnimal < 10 ){
                animais[contadorAnimal] = Animal.builder().nome("Animal").build();
                contadorAnimal++;
            }
            getDAO().createOrUpdate(
                Cliente.builder()
                    .nome("Cliente")
                    .animalList( Arrays.asList( animais ) )
                .build()
            );
            contadorCliente++;
        }
    }

}

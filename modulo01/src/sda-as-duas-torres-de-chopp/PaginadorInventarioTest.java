import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class PaginadorInventarioTest{
    @Test
    public void limitarComMarcadorMenorOuIgualAZero(){
        Item a = new Item("Abobora", 1);
        Item b = new Item("Batata", 2);
        Item c = new Item("Cebola", 3);
        Inventario inventario = new Inventario();
        inventario.adicionar(a);
        inventario.adicionar(b);
        inventario.adicionar(c);
        ArrayList<Item> resultadoEsperado = new ArrayList<Item>(2);
        resultadoEsperado.add(a);
        resultadoEsperado.add(b);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(-10);
        assertEquals(resultadoEsperado, paginador.limitar(2));
    }
    @Test
    public void limitarComMarcadorMaiorQueInventario(){
        Item a = new Item("Abobora", 1);
        Item b = new Item("Batata", 2);
        Item c = new Item("Cebola", 3);
        Inventario inventario = new Inventario();
        inventario.adicionar(a);
        inventario.adicionar(b);
        inventario.adicionar(c);
        ArrayList<Item> resultadoEsperado = new ArrayList<Item>(2);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(5);
        assertEquals(resultadoEsperado, paginador.limitar(2));
    }
    @Test
    public void limitarComNZero(){
        Item a = new Item("Abobora", 1);
        Item b = new Item("Batata", 2);
        Item c = new Item("Cebola", 3);
        Inventario inventario = new Inventario();
        inventario.adicionar(a);
        inventario.adicionar(b);
        inventario.adicionar(c);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        assertNull(paginador.limitar(0));
    }
    @Test
    public void limitarSituacaoPadrao(){
        Item a = new Item("Espada", 1);
        Item b = new Item("Escudo de metal", 2);
        Item c = new Item("Pocao de HP", 3);
        Item d = new Item("Bracelete", 1);
        Inventario inventario = new Inventario();
        inventario.adicionar(a);
        inventario.adicionar(b);
        inventario.adicionar(c);
        inventario.adicionar(d);
        ArrayList<Item> resultadoEsperado = new ArrayList<Item>(2);
        resultadoEsperado.add(c);
        resultadoEsperado.add(d);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(2);
        assertEquals(resultadoEsperado, paginador.limitar(2));
    }
    @Test
    public void limitarComNExcedendoInventario(){
        Item a = new Item("Espada", 1);
        Item b = new Item("Escudo de metal", 2);
        Item c = new Item("Pocao de HP", 3);
        Item d = new Item("Bracelete", 1);
        Inventario inventario = new Inventario();
        inventario.adicionar(a);
        inventario.adicionar(b);
        inventario.adicionar(c);
        inventario.adicionar(d);
        ArrayList<Item> resultadoEsperado = new ArrayList<Item>(5);
        resultadoEsperado.add(c);
        resultadoEsperado.add(d);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(2);
        assertEquals(resultadoEsperado, paginador.limitar(5));
    }
}
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.enums.Categoria;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.enums.MidiaType;
import br.com.dbc.locadora.rest.dto.MidiaDTO;
import br.com.dbc.locadora.service.MidiaService;
import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api/midia")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class MidiaRestController extends AbstractCrudRestController<Midia, Long, MidiaService> {
    
    @Autowired
    private MidiaService midiaService;

    @Override
    protected MidiaService getService() {
        return midiaService;
    }
    
    //com finalidade de testes
    @PostMapping("/saveFromDTO")
    public void post(@RequestBody MidiaDTO dto) {
        getService().saveFromDTO(dto, 16L);
    }
    @PostMapping("/saveFromDTOList")
    public void post(@RequestBody List<MidiaDTO> dtos) {
        getService().saveFromDTOList(dtos, 32L);
    }
    
    @GetMapping("/count/{tipo}")
    public ResponseEntity countByTipo(@PathVariable MidiaType tipo){
        return ResponseEntity.ok( getService().countByTipo(tipo) );
    }
    
    @GetMapping("/search")
    public ResponseEntity<?> search(Pageable pageable,
        
        @RequestParam(name = "titulo", required = false)
        String titulo,
        
        @RequestParam(name = "categoria", required = false)
        Categoria categoria,
        
        @RequestParam(name = "lancamentoIni", required = false)
        @DateTimeFormat(pattern = "dd/MM/yyyy")
        LocalDate lancamentoIni,
        
        @RequestParam(name = "lancamentoFim", required = false)
        @DateTimeFormat(pattern = "dd/MM/yyyy")
        LocalDate lancamentoFim
    
    ) {
        
        return ResponseEntity.ok(
            getService().search(titulo, categoria, lancamentoIni, lancamentoFim, pageable)
        );
    
    }
    
}
function formatarElfos(array){

  for(var elfo of array){

    elfo.nome = elfo.nome.toUpperCase()

    elfo.temExperiencia = ! ! elfo.experiencia
    delete elfo.experiencia;

    elfo.descricao = elfo.nome
    + ( elfo.temExperiencia ? " com" : " sem" )
    + " experiência e"
    + ( elfo.temExperiencia === ! elfo.qtdFlechas ? elfo.temExperiencia ? " sem" : " com" : "" )
    + ( elfo.qtdFlechas ? " " + elfo.qtdFlechas : "" )
    + ( (! elfo.qtdFlechas) && elfo.temExperiencia ? " sem" : "" )
    + " flecha"
    + ( elfo.qtdFlechas === 1 ? "" : "s" )

  }
}

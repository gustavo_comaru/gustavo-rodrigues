function naoBissexto(ano){

  if(typeof ano != "number")
    return undefined

  if(ano % 4 == 0 && ( ano % 100 != 0 || ano % 400 == 0))
    return false

  else
    return true

}

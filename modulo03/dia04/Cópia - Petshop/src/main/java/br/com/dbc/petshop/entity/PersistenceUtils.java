package br.com.dbc.petshop.entity;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class PersistenceUtils {

    private static EntityManager em;

    static {
        em = Persistence
                .createEntityManagerFactory("petshop")
                .createEntityManager();
    }

    public static EntityManager getEm() {
        return em;
    }

}

package br.com.dbc.locadora.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Aluguel extends AbstractEntity<Long> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate retirada;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate previsao;
    
    private LocalDateTime devolucao;
    
    private Double multa;
    
    private Long idCliente;

}

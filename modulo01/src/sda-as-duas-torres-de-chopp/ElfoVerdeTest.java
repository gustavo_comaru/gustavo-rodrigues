import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    @Test
    public void criarElfoVerde(){
        ElfoVerde marina = new ElfoVerde("Marina");
        assertTrue("Marina".equals(marina.getNome()));
    }
    @Test
    public void elfoVerdeGanhaItemPermitido(){
        ElfoVerde gerson = new ElfoVerde("Gerson, o filho de Ger");
        assertNull(gerson.getInventario().obter(2));
        gerson.ganharItem(new Item("Espada de aco valiriano", 1));
        assertTrue(gerson.getInventario().obter(2).equals(new Item("Espada de aco valiriano", 1)));
    }
    @Test
    public void elfoVerdeTentaGanharItemProibidoENaoConsegue(){
        ElfoVerde gregorio = new ElfoVerde("Gregorio");
        assertNull(gregorio.getInventario().obter(2));
        gregorio.ganharItem(new Item("Espada comum", 3));
        assertNull(gregorio.getInventario().obter(2));
    }
    @Test
    public void elfoVerdeAtiraUmaFlechaERecebeODobroDeExperiencia(){
        ElfoVerde edgar = new ElfoVerde("Edgar");
        edgar.atirarFlecha(new Dwarf("Antonio"));
        assertEquals(2, edgar.getExperiencia());
    }
    @Test
    public void elfoVerdetiraUmaFlechaEObtemSucessoEmFerirSeuAlvo(){
        ElfoVerde edgar = new ElfoVerde("Edgar");
        Dwarf antonio = new Dwarf("Antonio");
        edgar.atirarFlecha(antonio);
        assertEquals(100.0, antonio.getVida(), 0.1);
    }
    @Test
    public void elfoVerdeAtiraSeteVezes(){
        ElfoVerde edgar = new ElfoVerde("Edgar");
        for(int i = 0; i < 7; i++){
            edgar.atirarFlecha(new Dwarf("Antonio"));
        }
        assertEquals(0, edgar.getInventario().buscar("Flecha").getQuantidade());
        assertEquals(14, edgar.getExperiencia());
    }
    
}
package br.com.dbc.petshopjpa.service;

import br.com.dbc.petshopjpa.dao.ClienteDAO;
import br.com.dbc.petshopjpa.dao.PersistenceUtils;
import br.com.dbc.petshopjpa.entity.Animal;
import br.com.dbc.petshopjpa.entity.Cliente;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import org.mockito.internal.util.reflection.Whitebox;

public class ClienteServiceTest{
    
    public ClienteServiceTest(){
    }
    
    @BeforeClass
    public static void setUpClass(){
    }
    
    @AfterClass
    public static void tearDownClass(){
    }
    
    @Before
    public void setUp(){
        em.getTransaction().begin();
        em.createQuery("delete from Cliente").executeUpdate();
        em.getTransaction().commit();
    }
    
    @After
    public void tearDown(){
    }

    private final EntityManager em = PersistenceUtils.getEntityManager();
    
    @Test
    public void testFindAll(){
        System.out.println("findAll");
        em.getTransaction().begin();
        Cliente c = Cliente.builder()
            .nome("fulano")
            .animalList(
                Arrays.asList( Animal.builder()
                    .nome("rex")
                    .build()
                )
            )
            .build();
        em.persist(c);
        em.getTransaction().commit();
        ClienteService instance = new ClienteService();
        List<Cliente> clientes = instance.findAll();
        Assert.assertEquals(
            1,
            clientes.size()
        );
        Assert.assertEquals(
            c.getId(),
            clientes.stream().findFirst().get().getId()
        );
        Assert.assertEquals(
            c.getAnimalList(),
            clientes.stream().findFirst().get().getAnimalList()
        );
        Assert.assertEquals(
                c.getAnimalList().get(0).getId(),
                clientes.stream().findFirst().get().getAnimalList().get(0).getId()
        );
        
    }
    
    @Test
    public void testFindOneMocked(){
        ClienteService clienteService = new ClienteService();
        ClienteDAO clienteDAO = Mockito.mock(ClienteDAO.class);
        Whitebox.setInternalState(clienteService, "clienteDAO", clienteDAO);
        Cliente created = Cliente.builder()
            .nome("Geremias")
            .animalList(Arrays
                .asList(Animal.builder()
                    .nome("Bixin")
                    .build()
                )
            )
            .build();
        Mockito.doReturn(created).when(clienteDAO).findOne(created.getId());
        Cliente returned = clienteService.findOne(created.getId());
        Assert.assertEquals(
            created.getId(),
            returned.getId()
        );
        Mockito.verify(clienteDAO, times(1)).findOne(created.getId());
    }
    
}

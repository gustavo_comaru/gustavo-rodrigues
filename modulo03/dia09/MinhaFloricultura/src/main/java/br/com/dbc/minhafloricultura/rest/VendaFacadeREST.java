package br.com.dbc.minhafloricultura.rest;

import br.com.dbc.minhafloricultura.dao.VendaDAO;
import br.com.dbc.minhafloricultura.entity.Venda;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("venda")
public class VendaFacadeREST extends AbstractCrudREST<Venda, VendaDAO> {

    @Inject
    private VendaDAO vendaDAO;

    @Override
    protected VendaDAO getDAO() {
        return vendaDAO;
    }
    
}

function somarPares(array){
  var soma = 0
  var deveSomar = true
  for(var num of array){
    if(deveSomar)
      soma += num
    deveSomar = !deveSomar
  }
  return soma
}

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class Exercito{
    
    private int qtdSoldados = 0;

    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    private ArrayList<Elfo> soldados = new ArrayList<Elfo>();    
    
    public Exercito(){
        TIPOS_PERMITIDOS.add(ElfoVerde.class);
        TIPOS_PERMITIDOS.add(ElfoNoturno.class);
    }
    
    public ArrayList<Elfo> getSoldados(){
        return soldados;
    }
    
    public Elfo getSoldado(int pos){
        if(soldados.isEmpty() || pos >= soldados.size()) return null;
        return soldados.get(pos);
    }
   
    public int getQtdSoldado(){
        return qtdSoldados;
    }
    
    public boolean podeAlistar(Elfo elfo){
        if(elfo.getStatus() == Status.MORTO) return false;
        if(TIPOS_PERMITIDOS.contains(elfo.getClass())) return true;
        return false;
    }
    
    public void alistar(Elfo elfo){
        if(!podeAlistar(elfo)) return;
        soldados.add(elfo);
        ArrayList<Elfo> elfosDosStatus = porStatus.get(elfo.getStatus());
        if(elfosDosStatus == null){
            elfosDosStatus = new ArrayList<>();
            porStatus.put(elfo.getStatus(), elfosDosStatus);
        }
        elfosDosStatus.add(elfo);
        qtdSoldados++;
    }
    
    public ArrayList<Elfo> buscarStatus(Status statusProcurado){
        return this.porStatus.get(statusProcurado);
    }
    
    public ArrayList<Elfo> buscarStatusSeq(Status statusProcurado){
        ArrayList<Elfo> resultado = new ArrayList<Elfo>();
        int i = 0;
        while(i < soldados.size()){
            if(soldados.get(i).getStatus() == statusProcurado) resultado.add(soldados.get(i));
            i++;
        }
        return resultado;
    }
    
}
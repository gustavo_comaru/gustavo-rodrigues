package br.com.dbc.minhafloricultura.rest;

import br.com.dbc.minhafloricultura.dao.ProdutoDAO;
import br.com.dbc.minhafloricultura.entity.Produto;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("produto")
public class ProdutoFacadeREST extends AbstractCrudREST<Produto, ProdutoDAO> {

    @Inject
    private ProdutoDAO produtoDAO;

    @Override
    protected ProdutoDAO getDAO() {
        return produtoDAO;
    }
    
    /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findByDescription(@PathParam("descricao") String descricao) {
        return Response.ok( getDAO().findAll() ).build();
    }
    */
}

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoTest{
    @Test
    public void alistarUmElfoVerde(){
        Exercito exercito = new Exercito();
        ElfoVerde elfoVerde = new ElfoVerde("Elfo");
        assertNull(exercito.getSoldado(0));
        exercito.alistar(elfoVerde);
        assertTrue(exercito.getSoldados().contains(elfoVerde));
    }
    @Test
    public void alistarUmElfoNoturno(){
        Exercito exercito = new Exercito();
        ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo");
        assertNull(exercito.getSoldado(0));
        exercito.alistar(elfoNoturno);
        assertTrue(exercito.getSoldados().contains(elfoNoturno));
    }    
    @Test
    public void alistarUmElfoDaLuz(){
        Exercito exercito = new Exercito();
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Elfo");
        assertNull(exercito.getSoldado(0));
        exercito.alistar(elfoDaLuz);
        assertFalse(exercito.getSoldados().contains(elfoDaLuz));        
    }
    @Test
    public void alistarUmElfoComum(){
        Exercito exercito = new Exercito();
        Elfo elfo = new Elfo("Elfo");
        assertNull(exercito.getSoldado(0));
        exercito.alistar(elfo);
        assertFalse(exercito.getSoldados().contains(elfo));        
    }
    @Test
    public void alistarUmElfoPermitidoMorto(){
        Exercito exercito = new Exercito();
        ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo");
        assertNull(exercito.getSoldado(0));
        for(int i = 0; i < 7; i++){
           elfoNoturno.atirarFlecha(new Dwarf("Antonio"));
        }
        assertEquals(Status.MORTO, elfoNoturno.getStatus());
        exercito.alistar(elfoNoturno);
        assertNull(exercito.getSoldado(0));    
    }
    @Test
    public void buscarSoldadosPorStatusSequencialmente(){
        Exercito exercito = new Exercito();
        ElfoVerde elfoVerde = new ElfoVerde("Elfo");
        for(int i = 0; i < 10; i++){
            exercito.alistar(elfoVerde);
        }
        ArrayList<Elfo> listaObtida = exercito.buscarStatusSeq(Status.VIVO);
        for(int i = 0; i < 10; i++){
            assertEquals(elfoVerde, listaObtida.get(i));
        }
    }
    @Test
    public void buscarSoldadosPorStatusSequencialmenteMasMorto(){
        Exercito exercito = new Exercito();
        ElfoVerde elfoVerde = new ElfoVerde("Elfo");
        for(int i = 0; i < 9; i++){
            exercito.alistar(elfoVerde);
        }
        ElfoNoturno cadaverNoturno = new ElfoNoturno("Elfo");
        exercito.alistar(cadaverNoturno);
        for(int i = 0; i < 7; i++){
           cadaverNoturno.atirarFlecha(new Dwarf("Antonio"));
        }
        assertEquals(Status.MORTO, cadaverNoturno.getStatus());
        ArrayList<Elfo> listaObtida = exercito.buscarStatusSeq(Status.MORTO);
        assertEquals(cadaverNoturno, listaObtida.get(0));
    }
    @Test
    public void buscarVivosExistindo(){
        Elfo vivo = new ElfoVerde("Galadriel");
        Exercito exercito = new Exercito();
        exercito.alistar(vivo);
        ArrayList<Elfo> esperado = new ArrayList<>();
        esperado.add(vivo);
        assertEquals(esperado, exercito.buscarStatus(Status.VIVO));
    }    
    //@Test
    public void buscarMortosExistindo(){
        Exercito exercito = new Exercito();
        ElfoVerde elfoVerde = new ElfoVerde("Elfo");
        for(int i = 0; i < 9; i++){
            exercito.alistar(elfoVerde);
        }
        ElfoNoturno cadaverNoturno = new ElfoNoturno("Elfo");
        exercito.alistar(cadaverNoturno);
        for(int i = 0; i < 7; i++){
           cadaverNoturno.atirarFlecha(new Dwarf("Antonio"));
        }
        assertEquals(Status.MORTO, cadaverNoturno.getStatus());
        ArrayList<Elfo> listaObtida = exercito.buscarStatus(Status.MORTO);
        assertEquals(cadaverNoturno, listaObtida.get(0));
    }
}

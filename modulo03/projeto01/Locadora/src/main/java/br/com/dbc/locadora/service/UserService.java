package br.com.dbc.locadora.service;

import br.com.dbc.locadora.domain.User;
import br.com.dbc.locadora.repository.RoleRepository;
import br.com.dbc.locadora.repository.UserRepository;
import br.com.dbc.locadora.rest.dto.UserDTO;
import java.util.Arrays;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@PreAuthorize("hasAuthority('ADMIN_USER')")
@Transactional(readOnly = false, rollbackFor = Exception.class)
public class UserService {
        
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    public User create( UserDTO input ){
        if(userRepository.findByUsername(input.getUsername()).isPresent())
            return null;
        return userRepository.save(
            User.builder()
                .username(input.getUsername())
                .firstName(input.getFirstName())
                .lastName(input.getLastName())
                .password(passwordEncoder.encode(input.getPassword()))
                .roles(Arrays.asList(roleRepository.findById(1L).get()))
                .build()
        );
    }
    
    public User changePassword( UserDTO input ){
        Optional<User> userOpt = userRepository.findByUsername(input.getUsername());
        if(! userOpt.isPresent())
            return null;
        User user = userOpt.get();
        user.setPassword(passwordEncoder.encode(input.getPassword()));
        return userRepository.save(user);
    }
    
}
package br.com.dbc.locadora.rest.dto;

import br.com.dbc.locadora.entity.enums.MidiaType;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MidiaCatalogoDTO {
    private MidiaType tipo;
    private Double preco;
    private Boolean disponivel;
    private LocalDateTime dataPrevista;
}

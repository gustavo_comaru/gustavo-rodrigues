import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NumeroTest {
    @Test
    public void testar5Impar(){
        //Arrange
        Numero cinco = new Numero(5);
        //Act
        boolean resultado = cinco.impar();
        //Assert
        assertTrue(resultado);    
    }
    
    @Test
    public void numero27Impar(){
        assertTrue(new Numero(27).impar());
    } 

    @Test
    public void numero2018NaoImpar(){
        Numero n = new Numero(2018);
        assertFalse(n.impar());
    }
    
    @Test
    public void numero12NaoImpar(){
        Numero n = new Numero(12);
        assertFalse(n.impar());
    }
    
    @Test
    public void soma9Div1892376(){
        assertTrue(new Numero(9).verificarSomaDivisivel(1892376));
    }
    
    @Test
    public void soma25522556SDiv0(){
        assertTrue(new Numero(25522556).verificarSomaDivisivel(0));
    }
    
    @Test
    public void soma3Div17(){
        assertFalse(new Numero(3).verificarSomaDivisivel(17));
        
    }
}
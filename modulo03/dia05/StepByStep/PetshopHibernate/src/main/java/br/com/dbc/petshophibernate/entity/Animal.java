package br.com.dbc.petshophibernate.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "ANIMAL")
@NamedQueries({
    @NamedQuery(name = "Animal.findAll", query = "SELECT a FROM Animal a")})

//lombok:
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Animal extends AbstractEntity<Long> implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name = "ANIMAL_SEQ", sequenceName = "ANIMAL_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "ANIMAL_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;
    
    @Column(name = "NOME", nullable = false)
    private String nome;
    
    @ToString.Exclude
    @ManyToMany(mappedBy = "animalList")
    private List<Cliente> clienteList;

}

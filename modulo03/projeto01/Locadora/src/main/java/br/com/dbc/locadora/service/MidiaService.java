package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.enums.Categoria;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.enums.MidiaType;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.rest.dto.MidiaCatalogoDTO;
import br.com.dbc.locadora.rest.dto.MidiaDTO;
import br.com.dbc.locadora.rest.dto.MidiaOutDTO;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MidiaService extends AbstractCrudService<Midia, Long, MidiaRepository>{
        
    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private ValorMidiaService valorMidiaService;
    
    @Autowired
    private AluguelService aluguelService;
    
    @Autowired
    private FilmeService filmeService;

    @Override
    protected MidiaRepository getRepository(){
        return midiaRepository;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void saveFromDTOList( List<MidiaDTO> dtos, Long idFilme ){
        
        for( MidiaDTO dto : dtos ){
            saveFromDTO(dto, idFilme );
        }
        
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void saveFromDTO( MidiaDTO dto, Long idFilme ){
        
        MidiaType novoTipo = null;
        if(dto.getTipo() != null)
            novoTipo = MidiaType.valueOf( dto.getTipo() );
        
        Midia novaMidia = null;
        for( int i = 0; i < dto.getQuantidade(); i++ ){    
            
            novaMidia = Midia.builder()
                .tipo(novoTipo)
                .idFilme(idFilme)
                .build();
            
            Midia midiaSalva = getRepository().save(novaMidia);
            
            valorMidiaService.saveFromValorEMidia( dto.getValor(), midiaSalva.getId() );
            
        }
    }

    public Long countByTipo( MidiaType tipo ) {
        return midiaRepository.countByTipo(tipo);
    }
        
    public Long countByTipoAndIdFilme( MidiaType tipo, Long idFilme ) {
        return midiaRepository.countByTipoAndIdFilme(tipo, idFilme);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void retirarMidias( List<Long> ids, Long idAluguel ){
        for( Long id : ids ){
            midiaRepository.findById(id).get().setIdAluguel(idAluguel);
        }
    }
    
    public void devolverMidias( List<Long> ids ){
        for( Long id : ids ){
            midiaRepository.findById(id).get().setIdAluguel(null);
        }
    }
    
    public List<Midia> findByIdAluguel( Long idAluguel ){
        return midiaRepository.findByIdAluguel( idAluguel );
    }
    
    public Double findValorAtual( Long idMidia ){
        return valorMidiaService.findValorAtual( idMidia );
    }
    
    public Double findValorVigenteNoPeriodo( Long idMidia, LocalDateTime data ){
        return valorMidiaService.findValorPorIdMidiaEData( idMidia, data );
    }
    
    public boolean taAlugada( Long idMidia ){
        Midia midiaEmQuestao = midiaRepository.findById(idMidia).get();
        return ( midiaEmQuestao.getIdAluguel() != null );
    }
    
    public List<Midia> findByIdFilme( Long idFilme ){
        return midiaRepository.findByIdFilme(idFilme);
    }
    
    public void updateFromDTOList( List<MidiaDTO> dtos, Long idFilme ){
        for( MidiaDTO dto : dtos ){
            updateFromDTO( dto, idFilme );
        }
    }
    
    public void updateFromDTO( MidiaDTO dto, Long idFilme ){
        
        LocalDateTime data = LocalDateTime.now();
        
        MidiaType tipo = null;
        if(dto.getTipo() != null)
            tipo = MidiaType.valueOf( dto.getTipo() );
        
        List<Midia> midias = midiaRepository.findByIdFilmeAndTipo( idFilme, tipo );
        
        if( midias.size() > dto.getQuantidade() ){
            int faltam = midias.size() - dto.getQuantidade();
            for( Midia m : midias ){
                if( faltam > 0 && m.getIdAluguel() == null ){
                    midiaRepository.deleteById( m.getId() );
                    faltam--;
                }
            }
            if( faltam > 0 ){
                //nao temos midias suficientes para deletar
                //tomar providencias
            }
        }
        
        if( midias.size() < dto.getQuantidade() ){
            Midia novaMidia = null;
            int faltam = dto.getQuantidade() - midias.size();
            for( int i = 0; i < faltam; i++ ){    
                novaMidia = Midia.builder()
                    .tipo(tipo)
                    .idFilme(idFilme)
                    .build();
                Midia midiaSalva = getRepository().save(novaMidia);
        
                valorMidiaService.saveFromValorEMidiaEData( dto.getValor(), midiaSalva.getId(), data );
            
            }
        }
        
        for( Midia m : midias ){
            valorMidiaService.updateFromValorEIdMidia( dto.getValor(), m.getId(), data );
        }

    }
    
    public Page<MidiaOutDTO> search( String titulo, Categoria categoria, LocalDate ini, LocalDate fin, Pageable pageable ){
        return filmeService.searchMidia( titulo, categoria, ini, fin, pageable );
    }
    
    public List<MidiaCatalogoDTO> catalogarPorFilme( Long idFilme ){
        List<MidiaCatalogoDTO> resultado = new ArrayList<MidiaCatalogoDTO>(); 
        for(MidiaType tipo : MidiaType.values()){
            Midia midia = midiaRepository.findByIdFilmeAndTipo(idFilme, tipo).get(0);
            resultado.add(
                MidiaCatalogoDTO.builder()
                    .tipo(midia.getTipo())
                    .preco(valorMidiaService.acharValorAtualPorIdMidia(midia.getId()))
                    .disponivel(algumaMidiaDisponivel(idFilme, tipo))
                    .dataPrevista(algumaMidiaDisponivel(idFilme, tipo) ? null : aluguelService.acharProximaPrevisao(idFilme, tipo))
                    .build()
            );
        }
        return resultado;
    }
    
    public Boolean algumaMidiaDisponivel( Long idFilme, MidiaType tipo ){
        return midiaRepository.findFirstByIdFilmeAndTipoAndIdAluguel(idFilme, tipo, null).isPresent();
    }
    
    public List<Midia> acharTodasMidiasDesseFilmeETipo( Long idFilme, MidiaType tipo ){
        return midiaRepository.findByIdFilmeAndTipo(idFilme, tipo);
    }
    
}

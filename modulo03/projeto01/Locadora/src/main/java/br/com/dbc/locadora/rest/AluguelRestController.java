package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.rest.dto.AluguelInDTO;
import br.com.dbc.locadora.service.AluguelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/aluguel")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class AluguelRestController extends AbstractCrudRestController<Aluguel, Long, AluguelService> {
    
    @Autowired
    private AluguelService aluguelService;

    @Override
    protected AluguelService getService() {
        return aluguelService;
    }
    
    @PostMapping("/retirada")
    public ResponseEntity<?> retirar(@RequestBody AluguelInDTO dto) {
        return ResponseEntity.ok(getService().retirar(dto));
    }
    
    @PostMapping("/devolucao")
    public ResponseEntity<?> devolver(@RequestBody AluguelInDTO dto) {
        return ResponseEntity.ok( aluguelService.devolver(dto) );
    }
    
    @GetMapping("/devolucao")
    public ResponseEntity<?> devolucaoHoje(/*@PathVariable Pageable pageable*/) {
        return ResponseEntity.ok( aluguelService.previsaoHoje( /*pageable*/ ) );
    }
    
}
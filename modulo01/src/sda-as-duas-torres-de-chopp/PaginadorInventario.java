import java.util.ArrayList;

public class PaginadorInventario{
    
    private Inventario inventario;
    private int marcador;
    
    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
        this.marcador = 0;
    }
    
    public void pular(int marcador){
        if(marcador < 0){
            marcador = 0;
        }
        this.marcador = marcador;
    }
    
    public ArrayList<Item> limitar(int n){
        if(n <= 0){
            return null;
        }
        ArrayList<Item> resultado = new ArrayList<Item>(n);
        for(int i = marcador, cont = 0; i < inventario.size() && cont < n; i++){
            if(inventario.obter(i) != null){
                resultado.add(inventario.obter(i));
                cont++;
            }
        }
        return resultado;
    }
    
}
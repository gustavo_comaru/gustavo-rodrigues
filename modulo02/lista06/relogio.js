class Relogio{

  constructor( localTxt ){
    this.texto = localTxt
  }

  iniciar(){
    setInterval(this.exibir, 1000)
  }
  
  parar(){
    clearInterval(this.iniciar)
  }

  exibir(){
    document.getElementById(this.texto).innerText = new Date().toLocaleTimeString()
  }

}

const meuRelogio = new Relogio("txtHorario")
meuRelogio.iniciar()

const botaoPausar = document.getElementById("btnPausar")
//botaoPausar.onclick = meuRelogio.parar

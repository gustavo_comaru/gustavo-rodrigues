( function main() {

  const $formulario = document.getElementById("formulario")
  const $caixaInput = $formulario.querySelector("#inputNum")
  const $btnDaSorte = $formulario.querySelector("#btnSorte")

  const $dadosPokemon = document.getElementById("dadosPokemon")  
  const $h1 = $dadosPokemon.querySelector("h1")
  const $img = $dadosPokemon.querySelector("#thumb")
  const $id = $dadosPokemon.querySelector("#pkmId")
  const $altura = $dadosPokemon.querySelector("#pkmAltura")
  const $peso = $dadosPokemon.querySelector("#pkmPeso")

  const $tipos = $dadosPokemon.querySelector("#pkmTipos")

  const url = "https://pokeapi.co/api/v2/pokemon/"
  const api = new PokeApi(url)

  const carregarPokemon = function(id) {
    api.buscar(id)
    .then( res => res.json() )
    .then( dadosJson => {
      const novoPokemon = new Pokemon(dadosJson)
      renderizarPokemon(novoPokemon)
    } )
  }

  const carregarPokemonDaCaixa = function() {
    const id = $caixaInput.value
    carregarPokemon(id)
  }

  function renderizarPokemon(pokemon){
    $h1.innerText = pokemon.nome.toUpperCase()
    $img.src = pokemon.thumb
    $id.innerText = `ID: ${pokemon.id}`
    $altura.innerText = `Altura: ${pokemon.altura}cm`
    $peso.innerText = `Peso: ${pokemon.peso}kg`
    $tipos.innerText = `Tipos:`

    //for(var tipo of pokemon.tipos){
    //  var doc = document.createElement("li")
    //  doc.innerText = tipo
    //  $dadosPokemon.querySelector("#pkmTiposLista").appendChild(doc)
    //}

  }

  const carregarPokemonAleatorio = function(){
    var numSorteado = sortearId()
    while( sorteioEhRepetido(numSorteado) ){
      numSorteado = sortearId()
    }
    //adcionarPokemonNaListaNegra(numSorteado)
    carregarPokemon(id)
  }
  
  const sortearId = function(){
    const maiorNumeroPossivel = 802
    return id = Math.floor( (Math.random() * maiorNumeroPossivel) + 1 )
  }

  const sorteioEhRepetido = function(numSorteado){
    return getListaNegraStorage().includes(numSorteado)
  }

  const adcionarPokemonNaListaNegra = function(novoElemento){
    const listaNegra = getListaNegraStorage()
    if(! listaNegra.includes(novoElemento)) listaNegra.push(novoElemento)
    setListaNegraStorage(listaNegra)
  }
  
  $caixaInput.onblur = carregarPokemonDaCaixa
  $btnDaSorte.onclick = carregarPokemonAleatorio

} ) ()

//MUDAR:

const getListaNegraStorage = function(){
  const listaNegraString = localStorage.listaNegra
  return typeof listaNegraString === "string" ? JSON.parse("[" + listaNegraString + "]") : []
}

const setListaNegraStorage = function(arrayNegro){
  localStorage.listaNegra = JSON.stringify(arrayNegro)
}

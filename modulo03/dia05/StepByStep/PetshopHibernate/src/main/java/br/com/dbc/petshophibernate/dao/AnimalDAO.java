package br.com.dbc.petshophibernate.dao;

import br.com.dbc.petshophibernate.entity.Animal;

public class AnimalDAO extends AbstractDAO<Animal, Long> {
    
    private static final AnimalDAO instance;

    static {
        instance = new AnimalDAO();
    }
    
    public static final AnimalDAO getInstance(){
        return instance;
    }

    @Override
    protected Class<Animal> getEntityClass() {
        return Animal.class;
    }

    @Override
    protected String getIdProperty() {
        return "id";
    }
    
}

package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ValorMidiaService extends AbstractCrudService<ValorMidia, Long, ValorMidiaRepository>{
    
    @Autowired
    private ValorMidiaRepository valorMidiaRepository;
    
    @Override
    protected ValorMidiaRepository getRepository(){
        return valorMidiaRepository;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public ValorMidia saveFromValorEMidia( Double valor, Long idMidia ){
        
        LocalDateTime dataAgora = LocalDateTime.now();
        
        Optional<ValorMidia> valorAntigo = valorMidiaRepository.findByIdMidiaAndFinalVigencia(idMidia, null);
        if(valorAntigo.isPresent()){
            System.out.println("entrou");
            ValorMidia vm = valorAntigo.get();
            vm.setFinalVigencia(dataAgora);
            getRepository().save(vm);
        }
            
        ValorMidia novoValorMidia = ValorMidia.builder()
                .valor(valor)
                .inicioVigencia(dataAgora)
                .finalVigencia(null)
                .idMidia(idMidia)
                .build();

        return getRepository().save(novoValorMidia);
      
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public ValorMidia saveFromValorEMidiaEData( Double valor, Long idMidia, LocalDateTime data ){
    
        ValorMidia valorAntigo;
        valorAntigo = null;
        if(valorMidiaRepository.findByIdMidiaAndFinalVigencia(idMidia, null).isPresent())
            valorAntigo = valorMidiaRepository.findByIdMidiaAndFinalVigencia(idMidia, null).get();
        if( valorAntigo != null )
            valorAntigo.setFinalVigencia(data);
        

        ValorMidia novoValorMidia = ValorMidia.builder()
                .valor(valor)
                .inicioVigencia(data)
                .finalVigencia(null)
                .idMidia(idMidia)
                .build();
        
        return getRepository().save(novoValorMidia);
        
    }
    
    public Double findValorAtual( Long idMidia ){
        ValorMidia vm = valorMidiaRepository.findByIdMidiaAndFinalVigencia( idMidia, null ).get();
        if( vm == null ) return 999.999;
        return vm.getValor();
    }
    
    public List<ValorMidia> findByIdMidia( Long idMidia ){
        return valorMidiaRepository.findByIdMidia( idMidia );
    }
    
    public Double findValorPorIdMidiaEData( Long idMidia, LocalDateTime data ){
        return valorMidiaRepository.findByIdMidiaAndDataRetirada( idMidia, data ).getValor();
    }
    
    
    public ValorMidia updateFromValorEIdMidia( Double valor, Long idMidia, LocalDateTime agora ){
        
        ValorMidia valorMidia = valorMidiaRepository.findByIdMidiaAndFinalVigencia( idMidia, null ).get();
        
        if( valorMidia != null){
            valorMidia.setFinalVigencia(agora);
            valorMidiaRepository.save(valorMidia);
        }
        
        ValorMidia novoValorMidia = ValorMidia.builder()
            .valor( valor )
            .inicioVigencia( agora )
            .finalVigencia( null )
            .idMidia( idMidia )
            .build();
        
        return valorMidiaRepository.save( novoValorMidia );
        
    }
    
    public Double acharValorAtualPorIdMidia( Long idMidia ){
        return valorMidiaRepository.findByIdMidiaAndFinalVigencia(idMidia, null).get().getValor();
    }
    
}

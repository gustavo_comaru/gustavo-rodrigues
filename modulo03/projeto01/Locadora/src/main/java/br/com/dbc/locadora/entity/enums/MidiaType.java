package br.com.dbc.locadora.entity.enums;

public enum MidiaType {
    VHS, DVD, BLUE_RAY;
}

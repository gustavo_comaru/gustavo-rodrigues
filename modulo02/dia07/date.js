( function main(){

  const h1 = document.getElementById("horario")

  var relogioTitulo = setInterval( function(){ h1.innerText = new Date().toLocaleTimeString() }, 1000)
  relogioLog = setInterval( function(){ console.log(new Date()) }, 5000)

  const btnPararRelogio = document.getElementById("btnPararRelogio")
  btnPararRelogio.onclick = function() { clearInterval(relogioTitulo) }

} ) ()

import java.util.*;
import java.util.Map.Entry;

public class AgendaContatos{
    
    private HashMap<String, String> agenda = new LinkedHashMap<String, String>();
 
    public AgendaContatos(){
    }
    
    public HashMap<String, String> getMap(){
        return agenda;
    }
    
    public void adicionar(String nome, String telefone){
        this.agenda.put(nome, telefone);
    }
    
    public String obter(String nome){
        return this.agenda.get(nome);
    }
    
    public String buscar(String telefone){
        for(Entry<String, String> entry : agenda.entrySet()){
            String nomeDaVez = entry.getKey(); //getValue()
            if(obter(nomeDaVez).equals(telefone)){
                return nomeDaVez;
            }
        }
        return null;
    }
    
    public String csv(){
        boolean deveQuebrarLinha = false;
        if(agenda.isEmpty()) return "";
        StringBuilder result = new StringBuilder();
        String separador = System.lineSeparator();
        for(Entry<String, String> entry : agenda.entrySet()){
            String nomeDaVez = entry.getKey();
            if(deveQuebrarLinha) result.append(separador);
            result.append(String.format("%s,%s", nomeDaVez, obter(nomeDaVez)));
            deveQuebrarLinha = true;
        }
        return result.toString();
    }
    
}
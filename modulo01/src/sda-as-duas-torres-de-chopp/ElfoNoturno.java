public class ElfoNoturno extends Elfo{
    
    public ElfoNoturno(String nome){
        super(nome);
        MOD_EXP *= 3;
        QTD_DANO = 15.0;
    }
    
}

package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.AnimalDAO;
import br.com.dbc.petshophibernate.entity.Animal;

public class AnimalService extends AbstractCrudService<Animal, Long, AnimalDAO> {

    private static AnimalService instance;
    
    static{
        instance = new AnimalService();
    }
    
    public static AnimalService getInstance(){
        return instance;
    }
    
    @Override
    protected AnimalDAO getDAO() {
        return AnimalDAO.getInstance();
    }

}

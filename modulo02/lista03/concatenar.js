function concatenar(primeira, segunda){

  return "" + primeira + segunda

}

function concatenarSemUndefined(primeira = "", segunda = ""){

  //return `${ primeira} => ${ segunda }`
  return "" + primeira + segunda

}

function concatenarSemNull(primeira, segunda){

  if(primeira === null)
    primeira = ""

  if(typeof segunda === null)
    segunda = ""

  concatenar(primeira, segunda)

}

function concatenarEmPaz(primeira, segunda){

  if(typeof primeira === "undefined" || primeira === null)
    primeira = ""

  if(typeof segunda === "undefined" || segunda === null)
    segunda = ""

  concatenar(primeira, segunda)

}

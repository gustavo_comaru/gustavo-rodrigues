import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ElfoDaLuzTest{
    @Test
    public void criarElfoDaLuzQueNasceComEspada(){
        Elfo elfo = new ElfoDaLuz("Elfo Da Luz");
        assertTrue(new Item("Espada de Galvorn", 1).equals(elfo.getInventario().buscar("Espada de Galvorn")));
    }
    @Test
    public void atacaSoUmaVezEPerdeVida(){
        ElfoDaLuz elfo = new ElfoDaLuz("Elfo Da Luz");
        elfo.atacarComEspada(new Dwarf("Dwarf"));
        assertEquals(79.0, elfo.getVida(), 0.1);
    }
    @Test
    public void atacaSoUmaVezEMachucaDwarf(){
        ElfoDaLuz elfo = new ElfoDaLuz("Elfo Da Luz");
        Dwarf dwarf = new Dwarf("Dwarf");
        elfo.atacarComEspada(dwarf);
        assertEquals(100.0, dwarf.getVida(), 0.1);
    }    
    @Test
    public void atacaDuasVezesPerdeEDepoisGanhaVida(){
        ElfoDaLuz elfo = new ElfoDaLuz("Elfo Da Luz");
        elfo.atacarComEspada(new Dwarf("Dwarf"));
        elfo.atacarComEspada(new Dwarf("Dwarf"));
        assertEquals(89.0, elfo.getVida(), 0.1);
    }
    @Test
    public void atacaAteAntesDeMorrer(){
        ElfoDaLuz elfo = new ElfoDaLuz("Elfo Da Luz");
        int faltam = 16;
        while(faltam > 0){
            elfo.atacarComEspada(new Dwarf("Dwarf"));            
            faltam--;
        }
        assertEquals(12, elfo.getVida(), 0.1);
    }
    @Test
    public void atacaAteMorrer(){
        ElfoDaLuz elfo = new ElfoDaLuz("Elfo Da Luz");
        int faltam = 17;
        while(faltam > 0){
            elfo.atacarComEspada(new Dwarf("Dwarf"));            
            faltam--;
        }
        assertEquals(Status.MORTO, elfo.getStatus());
    }
    @Test
    public void atacaAteMorrerETentaMaisUmaVez(){
        ElfoDaLuz elfo = new ElfoDaLuz("Elfo Da Luz");
        int faltam = 17;
        while(faltam > 0){
            elfo.atacarComEspada(new Dwarf("Dwarf"));            
            faltam--;
        }
        Dwarf dwarf = new Dwarf("Dwarf");
        elfo.atacarComEspada(dwarf);
        assertEquals(110, dwarf.getVida(), 0.1);
    }
    @Test
    public void naoPodePerderEspada(){
        Elfo elfo = new ElfoDaLuz("Elfo");
        elfo.perderItem(new Item("Espada de Galvorn", 1));
        ArrayList<Item> esperado = new ArrayList<Item>();
        esperado.add(new Item("Arco", 1));
        esperado.add(new Item("Flecha", 7));
        esperado.add(new Item("Espada de Galvorn", 1));
        assertEquals(esperado, elfo.getInventario().getItens());
    }
    @Test
    public void podePerderOsOutrosItens(){
        Elfo elfo = new ElfoDaLuz("Elfo");
        elfo.perderItem(elfo.getInventario().buscar("Arco"));
        assertNull(elfo.getInventario().buscar("Arco"));
    }
    @Test
    public void naoPerdeAEspadaNemNaRaiz(){
        ElfoDaLuz elfo = new ElfoDaLuz("Elfo");
        elfo.getInventario().getItens().get(2).setQuantidade(0);
        elfo.atacarComEspada(new Dwarf("Dwarf"));
        assertEquals(new Item("Espada de Galvorn", 1), elfo.getInventario().obter(2));
        assertEquals(79, elfo.getVida(), 0.1);
    }
}
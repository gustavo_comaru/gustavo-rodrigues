public class DadoFake implements Sorteador{

    int valor;
    
    public DadoFake(int valor){
        this.valor = valor;
    }
    
    public void simular(int valor){
        this.valor = valor;
    }
    
    public int sortear(){
        return valor;
    }
}
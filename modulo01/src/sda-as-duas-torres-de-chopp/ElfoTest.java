import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    @Test
    public void elfoNasce(){
        Elfo elfo = new Elfo("Elfo");
        assertEquals("Elfo", elfo.getNome());
    }
    @Test
    public void elfoAtiraFlecha(){
        Elfo celeborn = new Elfo("Celeborn");
        celeborn.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(6, celeborn.getInventario().buscar("Flecha").getQuantidade());
        assertEquals(1, celeborn.getExperiencia());
    }
    @Test
    public void elfoEsgotaSuasFlechas(){
        Elfo erevan = new Elfo("Erevan"); 
        int aux = 1;
        while(erevan.getInventario().buscar("Flecha").getQuantidade() > 0){
            erevan.atirarFlecha(new Dwarf("Gimli"));
            assertEquals(erevan.getInventario().buscar("Flecha").getQuantidade(), 7-aux);
            assertEquals(erevan.getExperiencia(), aux);
            aux = aux + 1;
        }
    }
    @Test
    public void elfoAtiraOitoVezes(){
        Elfo legolas = new Elfo("Legolas");
        legolas.atirarFlecha(new Dwarf("Gimli"));
        legolas.atirarFlecha(new Dwarf("Gimli"));
        legolas.atirarFlecha(new Dwarf("Gimli"));
        legolas.atirarFlecha(new Dwarf("Gimli"));
        legolas.atirarFlecha(new Dwarf("Gimli"));
        legolas.atirarFlecha(new Dwarf("Gimli"));
        legolas.atirarFlecha(new Dwarf("Gimli"));
        legolas.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(0, legolas.getInventario().buscar("Flecha").getQuantidade());
        assertEquals(7, legolas.getExperiencia());               
    }
    @Test
    public void elfoAtiraEmDwarf(){
        Elfo legolas = new Elfo("Legolas");
        Dwarf gimli = new Dwarf("Gimli");
        legolas.atirarFlecha(gimli);
        assertEquals(6, legolas.getInventario().buscar("Flecha").getQuantidade());
        assertEquals(1, legolas.getExperiencia());
        assertEquals(100.0, gimli.getVida(), 0.1);
    }
    @Test
    public void verificarContadorGlobalDeElfos(){
        int esperado = Elfo.qtdTotalDeElfos + 1;
        Elfo jorge = new Elfo("Jorge");
        assertEquals(esperado, Elfo.qtdTotalDeElfos);
    }
    @Test
    public void verificarContadorGlobalDeElfosComSubclasses(){
        int esperado = Elfo.qtdTotalDeElfos + 3;
        ElfoVerde huguinho = new ElfoVerde("Hugo");
        ElfoNoturno zezinho = new ElfoNoturno("Jose");
        ElfoDaLuz luizinho = new ElfoDaLuz("Luiz");
        assertEquals(esperado, Elfo.qtdTotalDeElfos);
    }
}



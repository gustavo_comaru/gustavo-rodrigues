package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.AbstractEntity;
import br.com.dbc.locadora.service.AbstractCrudService;
import java.util.Objects;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

//@PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
public abstract class AbstractCrudRestController<E extends AbstractEntity, ID, S extends AbstractCrudService> {
    
    protected abstract S getService();
    
    @GetMapping()
    public ResponseEntity<?> list(Pageable pageable) {
        return ResponseEntity.ok( getService().findAll( pageable ) );
    }
    
    @GetMapping("/{id}")
    public ResponseEntity get(@PathVariable ID id) {
        return (ResponseEntity) getService().findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build() );
    }    
    
    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable ID id, @RequestBody E entity) {
        if(id == null || !Objects.equals(entity.getId(), id))
            return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(getService().save(entity));
    }
    
    @PostMapping
    public ResponseEntity<?> post(@RequestBody E entity) {
        if (entity.getId() != null)
            return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(getService().save(entity));
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable ID id) {
        getService().delete(id);
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Error message")
    public void handleError() {
    }
    
}

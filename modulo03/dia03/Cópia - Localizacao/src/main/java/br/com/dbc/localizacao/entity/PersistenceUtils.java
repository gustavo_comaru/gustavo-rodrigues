package br.com.dbc.localizacao.entity;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class PersistenceUtils {

    private static EntityManager em;

    static {
        em = Persistence
                .createEntityManagerFactory("localizacao_pu")
                .createEntityManager();
    }

    public static EntityManager getEm() {
        return em;
    }

}

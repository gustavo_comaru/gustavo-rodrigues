import java.util.*;

public class DadoD6 implements Sorteador{

    public int sortear(){
        Random aleatorios = new Random(System.currentTimeMillis());
        int numero =  (aleatorios.nextInt(6));
        if(numero < 0)
            numero = numero * -1;
        return numero + 1;
    }

}

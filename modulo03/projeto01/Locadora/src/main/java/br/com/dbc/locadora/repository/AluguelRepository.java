package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Aluguel;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AluguelRepository extends JpaRepository<Aluguel, Long> {
    
    public List<Aluguel> findByPrevisao( LocalDate previsao );
    
}

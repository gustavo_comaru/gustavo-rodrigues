describe( "fibonacci", function(){

  beforeEach( function(){
    chai.should()
  } )

  it( "primeiro numero da sequencia de fibonacci e 1", function(){
    fibo(1).should.equal(1)
  } )

  it( "segundo numero da sequencia de fibonacci e 1", function(){
    fibo(2).should.equal(1)
  } )

  it( "oitavo numero da sequencia de fibonacci e 21", function(){
    fibo(8).should.equal(21)
  } )

  it( "calcular numero da sequencia para inteiro negativo", function(){ } )

  it( "calcular numero da sequencia para numero quebrado", function(){ } )

  it( "calcular numero da sequencia para string", function(){ } )

} )

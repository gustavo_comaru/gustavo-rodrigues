public class DwarfBarbaLonga extends Dwarf{
   
   private Sorteador dado;
    
    public DwarfBarbaLonga(String nome){
       super(nome);
       dado = new DadoD6();
   }
   
   public DwarfBarbaLonga(String nome, Sorteador sorteador){
       super(nome);
       dado = sorteador;
   }
   
   @Override
   public void sofrerDano(){
       boolean deveSofrerDano = dado.sortear() <= 4;
       if(deveSofrerDano){
           vida -= QTD_DANO;
           if(vida <= 0){
               vida = 0;
               status = Status.MORTO;
           }
       }
   }
}

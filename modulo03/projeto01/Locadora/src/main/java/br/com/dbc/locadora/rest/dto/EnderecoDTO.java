package br.com.dbc.locadora.rest.dto;

import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnderecoDTO {
    
    @Size(min = 3, max = 100)
    private String rua;
    
    @Size(min = 3, max = 100)
    private String bairro;
    
    @Size(min = 3, max = 100)
    private String cidade;
    
    @Size(min = 3, max = 100)
    private String estado;
    
}

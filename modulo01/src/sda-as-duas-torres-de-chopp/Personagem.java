public abstract class Personagem{
    
    protected String nome;
    protected double vida;
    protected Status status;
    protected Inventario inventario;
    protected double QTD_DANO;
    
    {
        QTD_DANO = 0.0;
    }
    
    protected Personagem(String nome, double vida, Status status){
        this.nome = nome;
        this.vida = vida;
        this.status = status;
        this.inventario = new Inventario();
    }
    
    protected void setVida(double vida){
        this.vida = vida;
    }
    
    protected void setStatus(Status status){
        this.status = status;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public double getVida(){
        return this.vida;
    }
    
    public Status getStatus(){
        return status;
    }
    
    public Inventario getInventario(){
        return inventario;
    }
    
    public void ganharItem(Item novoItem){
        inventario.adicionar(novoItem);
    }
    
    public void perderItem(Item itemPerdido){
        inventario.remover(itemPerdido);
    }
    
    public void sofrerDano(){
        this.vida -= QTD_DANO;
        if(this.vida <= 0){
            this.vida = 0;
            this.status = Status.MORTO;
        }
    }
    
}
package br.com.dbc.locadora.rest.dto;

import br.com.dbc.locadora.entity.enums.Categoria;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FilmeCatalogoDTO {
    
    @Size(min = 3, max = 100)
    private String titulo;
    
    private Categoria categoria;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    private LocalDate dataDeLancamento;
    
    private List<MidiaCatalogoDTO> midiasDisponiveis;
    
}

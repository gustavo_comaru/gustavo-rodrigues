package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.enums.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.enums.MidiaType;
import br.com.dbc.locadora.rest.dto.FilmeDTO;
import br.com.dbc.locadora.service.FilmeService;
import br.com.dbc.locadora.service.MidiaService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api/filme")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class FilmeRestController extends AbstractCrudRestController<Filme, Long, FilmeService> {
    
    @Autowired
    private FilmeService filmeService;
    
    @Autowired
    private MidiaService midiaService;

    @Override
    protected FilmeService getService() {
        return filmeService;
    }
    
    @PostMapping("/midia")
    public ResponseEntity<?> post(@RequestBody FilmeDTO dto) {
        return ResponseEntity.ok(getService().saveFromDTO(dto));
    }
    
    @PutMapping("{id}/midia")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody FilmeDTO dto) {
        return ResponseEntity.ok(getService().updateFromDTO(id, dto));
    }
    
    @GetMapping("/count/{id}/{tipo}")
    public ResponseEntity countByTipo(@PathVariable Long id, @PathVariable MidiaType tipo){
        return ResponseEntity.ok( midiaService.countByTipoAndIdFilme(tipo, id) );
    }
    
    @GetMapping("/precos/{id}")
    public ResponseEntity historicoPrecos(@PathVariable Long id){
        return ResponseEntity.ok( filmeService.getHistoricoValores(id) );
    }
    
    @GetMapping("/search")
    public ResponseEntity<?> search(Pageable pageable,
        
        @RequestParam(name = "titulo", required = false)
        String titulo,
        
        @RequestParam(name = "categoria", required = false)
        Categoria categoria,
        
        @RequestParam(name = "lancamentoIni", required = false)
        @DateTimeFormat(pattern = "dd/MM/yyyy")
        LocalDate lancamentoIni,
        
        @RequestParam(name = "lancamentoFim", required = false)
        @DateTimeFormat(pattern = "dd/MM/yyyy")
        LocalDate lancamentoFim
    
    ) {
        
        return ResponseEntity.ok(
            getService().search(titulo, categoria, lancamentoIni, lancamentoFim, pageable)
        );
    
    }
    
    @PostMapping("/search/catalogo")
    public ResponseEntity<?> catalogar(Pageable pageable, @RequestBody FilmeDTO dto){
        return ResponseEntity.ok(filmeService.searchCatalogo(dto, pageable));
    }
    
}

CREATE TABLE CIDADE(
  ID NUMBER(38, 0) NOT NULL PRIMARY KEY,
  NOME VARCHAR(100) NOT NULL,
  SIGLA VARCHAR(100) NOT NULL
);

CREATE SEQUENCE CIDADE_SEQ
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE TABLE ESTADO(
  ID NUMBER(38,0) NOT NULL PRIMARY KEY,
  NOME VARCHAR(100) NOT NULL,
  SIGLA VARCHAR(100) NOT NULL
);

CREATE SEQUENCE ESTADO_SEQ
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

CREATE TABLE CIDADE_ESTADO(
  ID NUMBER(38, 0) NOT NULL PRIMARY KEY,
  ID_CIDADE NUMBER NOT NULL,
  ID_ESTADO NUMBER NOT NULL,
  FOREIGN KEY (ID_CIDADE)
    REFERENCES CIDADE(ID)
    ON DELETE CASCADE,
  FOREIGN KEY (ID_ESTADO)
    REFERENCES ESTADO(ID)
    ON DELETE CASCADE
);

CREATE SEQUENCE CIDADE_ESTADO_SEQ
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

INSERT INTO CIDADE
VALUES(
  CIDADE_SEQ.NEXTVAL,
  'Porto Alegre',
  'POA'
);

INSERT INTO CIDADE
VALUES(
  CIDADE_SEQ.NEXTVAL,
  'Santa Rosa',
  'STROS'
);

INSERT INTO CIDADE
VALUES(
  CIDADE_SEQ.NEXTVAL,
  'Sao Paulo',
  'SP'
);

INSERT INTO ESTADO
VALUES(
  ESTADO_SEQ.NEXTVAL,
  'Rio Grande Do Sul',
  'RS'
);

INSERT INTO ESTADO
VALUES(
  ESTADO_SEQ.NEXTVAL,
  'Sao Paulo',
  'SP'
);

INSERT INTO CIDADE_ESTADO(ID, ID_ESTADO, ID_CIDADE)
VALUES(
  CIDADE_ESTADO_SEQ.NEXTVAL,
  1,
  1
);

INSERT INTO CIDADE_ESTADO(ID, ID_ESTADO, ID_CIDADE)
VALUES(
  CIDADE_ESTADO_SEQ.NEXTVAL,
  1,
  2
);

INSERT INTO CIDADE_ESTADO(ID, ID_ESTADO, ID_CIDADE)
VALUES(
  CIDADE_ESTADO_SEQ.NEXTVAL,
  2,
  1
);

SELECT * FROM CIDADE_ESTADO CE
INNER JOIN CIDADE C ON C.ID = CE.ID_CIDADE
INNER JOIN ESTADO E ON E.ID = CE.ID_ESTADO;

DROP TABLE CIDADE;
DROP TABLE ESTADO;
DROP TABLE CIDADE_ESTADO;
DROP SEQUENCE CIDADE_SEQ;
DROP SEQUENCE ESTADO_SEQ;
DROP SEQUENCE CIDADE_ESTADO_SEQ;

SELECT * FROM CIDADE;
SELECT * FROM ESTADO;
SELECT * FROM CIDADE_ESTADO;

/*
CREATE TABLE PAIS(
  ID NUMBER(38,0) NOT NULL PRIMARY KEY,
  NOME VARCHAR2(100) NOT NULL,
  SIGLA VARCHAR2(100) NOT NULL
);

CREATE SEQUENCE PAIS_SEQ
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;

SELECT * FROM PAIS;
DROP TABLE PAIS;
*/




ROLLBACK;
COMMIT;
 
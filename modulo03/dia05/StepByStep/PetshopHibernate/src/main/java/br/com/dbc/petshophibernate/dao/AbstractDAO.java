package br.com.dbc.petshophibernate.dao;

// <ENTITY> -> GENERIC ( PADRAO: <T> )

import br.com.dbc.petshophibernate.dao.HibernateUtil;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.net.aso.e;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;




public abstract class AbstractDAO<ENTITY, ID> {
    
    private static final Logger LOG = Logger.getLogger(AbstractDAO.class.getName());
    
    protected abstract Class<ENTITY> getEntityClass();
    protected abstract String getIdProperty();
    
    public AbstractDAO(){};

    public ENTITY findById(ID id){
            Session session = null;
            try {
                session = HibernateUtil.getSessionFactory()
                        .openSession();
                return (ENTITY) session // mesmo com o return, o finally será executado
                        .createCriteria(getEntityClass())
                        .add(Restrictions.eq( getIdProperty(), id) )
                        .uniqueResult();
            }
            catch (HibernateException ex) {
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
                throw ex;
            }
            finally {
                if( session != null )
                    session.close();
            }
    }
    
    public void createOrUpdate(ENTITY e){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(e);
            transaction.commit();
        } catch(Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            throw ex;
        } finally {
            if(session != null)
                session.close();
        }   
    }
        
    public void delete(ENTITY e){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(e);
            transaction.commit();
        } catch(Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            throw ex;
        } finally {
            if(session != null)
                session.close();
        }    
    }

    public List<ENTITY> findAll(){
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            return session.createCriteria(getEntityClass()).list();
        } catch(Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            throw ex;
        } finally {
            if(session != null)
                session.close();
        }
    }
    
}

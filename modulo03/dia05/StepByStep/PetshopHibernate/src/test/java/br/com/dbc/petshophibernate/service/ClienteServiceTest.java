package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.ClienteDAO;
import br.com.dbc.petshophibernate.dao.HibernateUtil;
import br.com.dbc.petshophibernate.entity.Animal;
import br.com.dbc.petshophibernate.entity.Cliente;
import java.util.Arrays;
import java.util.List;
import javassist.NotFoundException;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ClienteServiceTest {
    
    public ClienteServiceTest(){}
    
    @BeforeClass
    public static void setUpClass(){}
    
    @AfterClass
    public static void tearDownClass(){}
    
    @Before
    public void setUp(){}
    
    @After
    public void tearDown(){}

    @Test
    public void testCreate(){
        System.out.println("create");
        Cliente cliente1 = Cliente.builder()
                .nome("Cliente1")
                .animalList(
                    Arrays.asList( Animal.builder()
                        .nome("Animal1")
                        .build()
                    )
                )
                .build();
        ClienteService.getInstance().create(cliente1);
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Cliente> clientes = session.createCriteria(Cliente.class).list();
        Cliente result = clientes.stream().findAny().get();
        Assert.assertEquals(
                "Quantidade de clientes errada",
                1,
                clientes.size()
        );
        Assert.assertEquals(
                "Cliente diferente",
                cliente1.getId(),
                result.getId()
        );
        Assert.assertEquals(
                "Quantidade de animais errada",
                cliente1.getAnimalList().size(),
                result.getAnimalList().size()
        );
        Assert.assertEquals(
                "Animal diferente",
                cliente1.getAnimalList().stream().parallel().findAny().get().getId(), //cliente1.getAnimalList().stream().findAny().get()
                result.getAnimalList().stream().findAny().get().getId()
        );
        session.close();
    }
    
    @Test
    public void testCreateMocked(){
        System.out.println("create mocked");
        Cliente cliente1 = Cliente.builder()
            .nome("Cliente1")
            .animalList(
                Arrays.asList(
                    Animal.builder()
                        .nome("Animal1")
                        .build()
                )
            )
            .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(cliente1);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.create(cliente1);
        verify(daoMock, times(1) ).createOrUpdate(cliente1);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCreateException(){
        System.out.println("create exception");
        ClienteService.getInstance()
            .create(
                Cliente.builder().id(1l).build()
            );
    }
    
    @Test
    public void testUpdateMocked(){
        System.out.println("update mocked");
        Cliente cliente1 = Cliente.builder()
            .id(123l)
            .nome("Cliente1")
            .animalList(
                Arrays.asList(
                    Animal.builder()
                        .nome("Animal1")
                        .build()
                )
            )
            .build();
        
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(cliente1);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.update(cliente1);
        verify(daoMock, times(1) ).createOrUpdate(cliente1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateException(){
        System.out.println("update exception");
        ClienteService.getInstance()
            .update(
                Cliente.builder().build()
            );
    }
    
    @Test
    public void testDeleteMockedIdNotNullEntityNotNull() throws Exception {
        System.out.println("delete mocked (id not null) (entity not null)");
        Cliente cliente1 = Cliente.builder()
            .id(123l)
            .nome("Fulano")
            .animalList(
                Arrays.asList(
                    Animal.builder()
                        .nome("Totó")
                        .build()
                )
            )
            .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).delete(cliente1);
        Mockito.doReturn(cliente1).when(daoMock).findById(cliente1.getId());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.delete(cliente1.getId());
        Mockito.verify(daoMock, times(1)).findById(cliente1.getId());
        Mockito.verify(daoMock, times(1)).delete(cliente1);
    }
    
    @Test(expected = NotFoundException.class)
    public void testDeleteMockedIdNotNullEntityNull() throws Exception {
        System.out.println("delete mocked (id not null) (entity null)");
        Cliente cliente1 = Cliente.builder()
            .id(123l)
            .nome("Fulano")
            .animalList(
                Arrays.asList(
                    Animal.builder()
                        .nome("Totó")
                        .build()
                )
            )
            .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).delete(cliente1);
        Mockito.doReturn(null).when(daoMock).findById(cliente1.getId());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.delete(cliente1.getId());
        Mockito.verify(daoMock, times(1)).findById(cliente1.getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteMockedIdNull() throws Exception {
        System.out.println("delete mocked (id null)");
        ClienteService.getInstance()
            .delete(null);
    }
    
}

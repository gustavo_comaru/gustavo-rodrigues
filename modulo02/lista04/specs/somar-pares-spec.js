describe( "somarPares", function(){

  beforeEach( function(){
    chai.should()
  } )

  it( "somar [ 1, 2, 3, 4, 5, 6 ] deve resultar 9", function(){
    var array = [ 1, 2, 3, 4, 5, 6 ]
    var resultado = somarPares(array)
    resultado.should.equal(9)
  } )

  it( "somar [ -10, 2, 6, 7, 3.14, 0, 0 ] deve resultar -0.86", function() {
    var array = [ -10, 2, 6, 7, 3.14, 0, 0 ]
    var resultado = somarPares(array)
    resultado.should.be.approximately(-0.86, 0.1)
  } )

  it( "somar [ 1, 56, 4.34, 6, -2 ] deve resultar 3.34", function() {
    var array = [ 1, 56, 4.34, 6, -2 ]
    var resultado = somarPares(array)
    resultado.should.be.approximately(3.34, 0.1)
  } )

} )

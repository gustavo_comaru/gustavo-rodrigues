package br.com.dbc.locadora.rest.dto;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.enums.MidiaType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MidiaOutDTO {
    
    private Long id;
    private MidiaType tipo;
    private Double valor;
    private Filme filme;
    
}

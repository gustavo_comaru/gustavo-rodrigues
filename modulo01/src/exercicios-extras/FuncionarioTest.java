import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

//PascalCase
public class FuncionarioTest {
    @Test
    public void capitaoAmericaDeveLucrar585(){
        Funcionario cap = new Funcionario("Capitão América", 500.0, 1000.0);
        assertEquals(cap.getLucro(), 585.0, 0.1);
    }
    
    @Test
    //camelCase
    public void rogueDeveLucrar1508(){
        assertEquals(1508.4675, new Funcionario("Rogue", 500.0, 7840.5).getLucro(), 0.1);
    }
}

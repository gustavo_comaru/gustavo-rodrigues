package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService extends AbstractCrudService<Cliente, Long, ClienteRepository>{
        
    @Autowired
    private ClienteRepository clienteRepository;
    
    @Autowired
    private CepService soapConnector;

    @Override
    protected ClienteRepository getRepository(){
        return clienteRepository;
    }
    
    public Object consultarCep(String cep){
        Object o = soapConnector.consultarCepPorString(cep);
        o = soapConnector.formatarCep(o);
        return o;
    }
    
}

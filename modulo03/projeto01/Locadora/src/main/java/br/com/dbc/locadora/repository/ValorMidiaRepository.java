package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.ValorMidia;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ValorMidiaRepository extends JpaRepository<ValorMidia, Long> {
    
    public Optional<ValorMidia> findByIdMidiaAndFinalVigencia( Long idMidia, LocalDateTime finalVigencia );
    public List<ValorMidia> findByIdMidia( Long idMidia );
    
    @Query("select vm from ValorMidia vm where vm.idMidia = :idMidia and vm.inicioVigencia >= :retirada and (vm.finalVigencia <= :retirada or vm.finalVigencia is null)")
    public ValorMidia findByIdMidiaAndDataRetirada(@Param("idMidia")Long idMidia, @Param("retirada")LocalDateTime retirada);
    
}

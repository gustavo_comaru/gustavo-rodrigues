package br.com.dbc.minhafloricultura.rest;

import br.com.dbc.minhafloricultura.dao.ClienteDAO;
import br.com.dbc.minhafloricultura.entity.Cliente;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("cliente")
public class ClienteFacadeREST extends AbstractCrudREST<Cliente, ClienteDAO> {

    @Inject
    private ClienteDAO clienteDAO;

    @Override
    protected ClienteDAO getDAO() {
        return clienteDAO;
    }
    
}

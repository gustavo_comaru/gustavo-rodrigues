var expect = chai.expect

describe( "formatarElfos", function(){

  beforeEach( function(){
    chai.should()
  } )

  it( "sem experiência e com flechas / com experiência e com flecha", function(){
    const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 6 }
    const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
    const arrayElfos = [ elfo1, elfo2 ]
    formatarElfos( arrayElfos )

    const esperado = 
    [
      {
        nome: "LEGOLAS",
        temExperiencia: false,
        qtdFlechas: 6,
        descricao: "LEGOLAS sem experiência e com 6 flechas"
      },
      {
        nome: "GALADRIEL",
        temExperiencia: true,
        qtdFlechas: 1,
        descricao: "GALADRIEL com experiência e 1 flecha"
      }
    ]

    expect(arrayElfos).to.eql(esperado)

  } )

} )

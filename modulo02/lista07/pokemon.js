class Pokemon {
  constructor( jsonApi ) {
    this.nome = jsonApi.name
    this.id = jsonApi.id
    this.thumb = jsonApi.sprites.front_default
    this.altura = jsonApi.height * 10 //cm
    this.peso = jsonApi.weight / 10 //kg
    this.tipos = jsonApi.types.map( t => t.type.name )
  }
}

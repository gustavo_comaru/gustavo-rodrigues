package br.com.dbc.locadora.entity.enums;

public enum Categoria {
    ACAO, AVENTURA, ANIMACAO;
}

export default class Pokemon{
  constructor( jsonVindoDaApi ) {
    this._nome = jsonVindoDaApi.name
    this.id = jsonVindoDaApi.id
    this.thumb = jsonVindoDaApi.sprites.front_default
    this._altura = jsonVindoDaApi.height
    this._peso = jsonVindoDaApi.weight
    this.tipos = jsonVindoDaApi.types.map( t => t.type.name )
    this.estatisticas = jsonVindoDaApi.stats.map( e => ( { nome: e.stat.name, valor: e.base_stat } ) )

  }

  get nome(){
    return this._nome.toUpperCase()
  }

  get altura(){
    return this._altura * 10
  }

  get peso(){
    return this._peso / 10
  }

}

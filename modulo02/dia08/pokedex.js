( function main() {

  const $formulario = document.getElementById("formulario")
  const $caixaInput = $formulario.querySelector("input")
  const $buttonGo = $formulario.querySelector("button")

  const $dadosPokemon = document.getElementById("dadosPokemon")  
  const $h1 = $dadosPokemon.querySelector("h1")
  const $img = $dadosPokemon.querySelector("#thumb")

  const $id = $dadosPokemon.querySelector("#pkmId")
  const $altura = $dadosPokemon.querySelector("#pkmAltura")
  const $peso = $dadosPokemon.querySelector("#pkmPeso")

  const $tipos = $dadosPokemon.querySelector("#pkmTipos")

  const carregarPokemon = function() {
  
    var dadosBanco = fetch(`https://pokeapi.co/api/v2/pokemon/${ $caixaInput.value }/`)

    dadosBanco
    .then( res => res.json() )
    .then( dadosJson => {

      $h1.innerText = dadosJson.name.toUpperCase()
      $img.src = dadosJson.sprites.front_default
      $id.innerText = `ID: ${dadosJson.id}`
      $altura.innerText = `Altura: ${dadosJson.height * 10}cm`
      $peso.innerText = `Peso: ${dadosJson.weight / 1000}kg`
      $tipos.innerText = "Tipos: "
    
    } )
    
  }

  $buttonGo.onclick = carregarPokemon

} ) ()

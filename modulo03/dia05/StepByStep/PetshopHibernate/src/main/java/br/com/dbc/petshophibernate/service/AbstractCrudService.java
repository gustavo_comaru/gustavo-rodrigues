//!!!
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.AbstractDAO;
import br.com.dbc.petshophibernate.entity.AbstractEntity;
import java.util.List;
import javassist.NotFoundException;

public abstract class AbstractCrudService<ENTITY extends AbstractEntity<ID>, 
        ID, DAO extends AbstractDAO<ENTITY, ID>> {

    protected abstract DAO getDAO();

    public List<ENTITY> findAll() {
        return getDAO().findAll();
    }

    public ENTITY findOne(ID id) {
        System.out.println("Envia email que buscou o id: " + id);
        return getDAO().findById(id);
    }

    public void create(ENTITY entity) {
        if (entity.getId() != null) {
            throw new IllegalArgumentException("Criação de cliente não pode ter ID");
        }
        getDAO().createOrUpdate(entity);
    }

    public void update(ENTITY entity) {
        if (entity.getId() == null) {
            throw new IllegalArgumentException("Atualização de cliente deve ter ID");
        }
        getDAO().createOrUpdate(entity);
    }

    public void delete(ID id) throws NotFoundException {
        if (id == null) {
            throw new IllegalArgumentException("ID deve ser informado");
        }
        ENTITY entity = findOne(id);
        if (entity == null) {
            throw new NotFoundException("Cliente não encontrado");
        }
        getDAO().delete(entity);
    }

}

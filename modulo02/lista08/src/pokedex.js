const urlApi = "https://pokeapi.co/api/v2/pokemon"
const pokeApi = new PokeApi(urlApi)

let pokedex = new Vue( {
  
  el: '#pokedex',
  
  data: {
    inputCaixa: '',
    pokemon: { },
    pokemonFoiCarregado: false,
    maiorIdPossivel: 802
  },
  
  methods: {
    
    receberInputCaixa(){
      const idBuscado = this.inputCaixa
      if( this.buscaEhValida(idBuscado) )
        this.buscar(idBuscado)
      else
        this.recusarInput()
    },

    recusarInput(){
      alert("ID INVALIDO")
    },

    buscaEhValida(id){
      return this.ehInteiro(id) && id > 0 && id <= this.maiorIdPossivel
    },

    ehMesmoId(id){
      return parseInt(id) === this.pokemon.id
    },

    ehInteiro(num){
      return parseInt(num) === parseFloat(num) && parseInt(num) !== NaN
    },

    async buscar(id){
      if(! this.ehMesmoId(id))
        this.pokemon = await pokeApi.buscar(id)
      this.pokemonFoiCarregado = true
    },

    buscarAleatorio(){
      idSorteado = this.sortearIdInedito()
      this.adicionarIdNaListaNegra(idSorteado)
      this.buscar(idSorteado) 
    },
    
    sortearId(){
      const maiorNumeroPossivel = this.maiorIdPossivel
      return id = Math.floor( (Math.random() * maiorNumeroPossivel) + 1 )
    },

    sortearIdInedito(){
      var idSorteado = this.sortearId()
      while( this.sorteioEhRepetido(idSorteado) ){
        idSorteado = this.sortearId()
      }
      return idSorteado
    },

    sorteioEhRepetido(idSorteado){
      return this.getListaNegraStorage().includes(idSorteado)
    },

    adicionarIdNaListaNegra(id){
      const listaNegra = this.getListaNegraStorage()
      if(! listaNegra.includes(id)){
        listaNegra.push(id)
        this.setListaNegraStorage(listaNegra)
      }
    },

    getListaNegraStorage(){
      const listaNegraString = localStorage.idsDePkmsJaSorteados
      return typeof listaNegraString === "string" ? JSON.parse(listaNegraString) : []
    },

    setListaNegraStorage(listaNegraArray){
      if(listaNegraArray.length < this.maiorIdPossivel)
        localStorage.idsDePkmsJaSorteados = JSON.stringify(listaNegraArray)
      else
        localStorage.idsDePkmsJaSorteados = JSON.stringify( [ ] )
    }

  }

} )

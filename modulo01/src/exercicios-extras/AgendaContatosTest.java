import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest{
    @Test
    public void adicionarEObterContato(){
        AgendaContatos contatos = new AgendaContatos();
        contatos.adicionar("Carlota", "1234-4321");
        String esperado = "1234-4321";
        assertTrue(esperado.equals(contatos.obter("Carlota")));
    }
    @Test
    public void buscarPeloTelefoneExistente(){
        AgendaContatos contatos = new AgendaContatos();
        contatos.adicionar("Joaquina", "1234-5678");
        String esperado = "Joaquina";
        assertTrue(esperado.equals(contatos.buscar("1234-5678")));
    }
    @Test
    public void buscarPeloTelefoneNaoExistente(){
        AgendaContatos contatos = new AgendaContatos();
        assertNull(contatos.buscar("1234-5678"));
    }
    @Test
    public void converterParaCSV(){
        String separador = System.lineSeparator();
        AgendaContatos contatos = new AgendaContatos();
        contatos.adicionar("Jurandir", "9999-8888");
        contatos.adicionar("Joshua", "2122-2324");
        contatos.adicionar("Gabe", "0000-1111");
        String resultadoString = String.format("Jurandir,9999-8888%sJoshua,2122-2324%sGabe,0000-1111", separador, separador);
        assertTrue(resultadoString.equals(contatos.csv()));
    }
}
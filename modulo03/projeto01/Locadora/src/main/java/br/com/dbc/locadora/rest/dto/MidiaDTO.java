package br.com.dbc.locadora.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MidiaDTO {
    
    private String tipo;
    private int quantidade;
    private Double valor;
    
}

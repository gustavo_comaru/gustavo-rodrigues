package br.com.dbc.locadora.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional( readOnly = true, rollbackFor = Exception.class )
public abstract class AbstractCrudService<E, ID, REP extends JpaRepository<E, ID>> {
    
    protected abstract REP getRepository();
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public E save( E entity ){
        return getRepository().save(entity);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete( ID id ){
        getRepository().deleteById(id);
    }
    
    public Optional<E> findById( ID id ){
        return getRepository().findById(id);
    }

    public Page<E> findAll( Pageable pa ){
        return getRepository().findAll( pa );
    }
    
}

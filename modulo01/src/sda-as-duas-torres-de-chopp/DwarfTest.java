import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    @Test
    public void dwarfNasceComNome(){
        Dwarf dwarf = new Dwarf("Epaminondas");
        assertEquals("Epaminondas", dwarf.getNome());
    }
    @Test
    public void dwarfNasceComStatusVivo(){
        Dwarf dwarf = new Dwarf("Carlos");
        assertEquals(Status.VIVO, dwarf.getStatus());
    }
    @Test
    public void dwarfSofreDano(){
        Dwarf joshua = new Dwarf("Joshua");
        joshua.sofrerDano();
        assertEquals(100.0, joshua.getVida(), 0.1);
    }
    @Test
    public void dwarfMorre(){
        Dwarf gimli = new Dwarf("Gimli");
        int qtdDePorradas = 0;
        while(qtdDePorradas < 11){
            gimli.sofrerDano();
            qtdDePorradas++;
        }
        assertEquals(Status.MORTO, gimli.getStatus());
    }
    @Test
    public void dwarfMortoSofreDano(){
        Dwarf tyrion = new Dwarf("Tyrion");
        int qtdDePorradas = 0;
        while(qtdDePorradas < 12){
            tyrion.sofrerDano();
            qtdDePorradas++;
        }
        assertEquals(0.0, tyrion.getVida(), 0.1);
    }
}
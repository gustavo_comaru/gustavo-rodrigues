import java.util.ArrayList;

public class ElfoVerde extends Elfo{
    
    private final ArrayList<String> DESCRICOES_PERMITIDAS = new ArrayList<String>();
    
    public ElfoVerde(String nome){
        super(nome);
        MOD_EXP *= 2;
        DESCRICOES_PERMITIDAS.add("Espada de aco valiriano");
        DESCRICOES_PERMITIDAS.add("Arco de Vidro");
        DESCRICOES_PERMITIDAS.add("Flecha de Vidro");
    }

    public void ganharItem(Item novo){
        if(DESCRICOES_PERMITIDAS.contains(novo.getDescricao())){ 
            inventario.adicionar(novo);
        }
    }
    
}

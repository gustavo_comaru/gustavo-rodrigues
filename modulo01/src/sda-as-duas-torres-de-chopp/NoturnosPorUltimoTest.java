import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class NoturnosPorUltimoTest {
    @Test
    public void recebeOrdemDeAtaqueDeveConstarVerdesDepoisNoturnos(){
        List<Elfo> soldados = new ArrayList();
        
        Elfo joao = new ElfoVerde("Joao");
        Elfo noturno = new ElfoNoturno("Meia Noite");
        
        soldados.add(noturno);
        soldados.add(joao);
        soldados.add(noturno);
        soldados.add(joao);
        soldados.add(noturno);
        soldados.add(joao);
        soldados.add(joao);
        soldados.add(noturno);
        soldados.add(noturno);
        soldados.add(joao);
        soldados.add(joao);
        soldados.add(noturno);
        soldados.add(joao);
        soldados.add(noturno);
        soldados.add(joao);
        soldados.add(noturno);
        soldados.add(noturno);
        soldados.add(noturno);
        soldados.add(joao);
        soldados.add(joao);
        soldados.add(noturno);
        soldados.add(noturno);
        soldados.add(joao);
        soldados.add(noturno);
        soldados.add(joao);
    
        NoturnosPorUltimo listaDeAtaque = new NoturnosPorUltimo();
        
        soldados = listaDeAtaque.getOrdemDeAtaque(soldados);
        
        List<Elfo> esperado = new ArrayList<Elfo>();
        
        for(int i = 0; i < 12; i++){
            esperado.add(joao);
        }
        
        for(int i = 0; i < 13; i++){
            esperado.add(noturno);
        }
        
        int x = 0;
        
        assertEquals(esperado, soldados);
    
    }
}

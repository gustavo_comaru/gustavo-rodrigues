    package br.com.dbc.locadora.rest.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AluguelInDTO {
    
    private Long idCliente;
    private List<Long> midias;
    
}

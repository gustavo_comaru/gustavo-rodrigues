import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest{
    @Test
    public void criarElfoNoturno(){
        ElfoNoturno juca = new ElfoNoturno("Juca");
        assertTrue("Juca".equals(juca.getNome()));
    }
    @Test
    public void elfoNoturnoAtiraUmaFlechaERecebeOTriploDeExperiencia(){
        ElfoNoturno edgar = new ElfoNoturno("Edgar");
        edgar.atirarFlecha(new Dwarf("Antonio"));
        assertEquals(3, edgar.getExperiencia());
    }
    @Test
    public void elfoNoturnoAtiraUmaFlechaEPerdeQuinzePontosDeVida(){
        ElfoNoturno edgar = new ElfoNoturno("Edgar");
        edgar.atirarFlecha(new Dwarf("Antonio"));
        assertEquals(85.0, edgar.getVida(), 0.1);
    }
    @Test
    public void elfoNoturnoAtiraUmaFlechaEObtemSucessoEmFerirSeuAlvo(){
        ElfoNoturno edgar = new ElfoNoturno("Edgar");
        Dwarf antonio = new Dwarf("Antonio");
        edgar.atirarFlecha(antonio);
        assertEquals(100.0, antonio.getVida(), 0.1);
    }
    @Test
    public void elfoNoturnoSeMataComSeteFlechadas(){
        ElfoNoturno edgar = new ElfoNoturno("Edgar");
        for(int i = 0; i < 7; i++){
            edgar.atirarFlecha(new Dwarf("Antonio"));
        }
        assertEquals(0, edgar.getInventario().buscar("Flecha").getQuantidade());
        assertEquals(21, edgar.getExperiencia());
        assertEquals(Status.MORTO, edgar.getStatus());
        assertEquals(0, edgar.getVida(), 0.1);
    }
    @Test
    public void elfoNoturnoTentaAtirarOitoVezes(){
        ElfoNoturno edgar = new ElfoNoturno("Edgar");
        //obtendo flecha por meio artificial:
        edgar.getInventario().buscar("Flecha").setQuantidade(8);
        for(int i = 0; i < 8; i++){
            edgar.atirarFlecha(new Dwarf("Antonio"));
        }
        assertEquals(1, edgar.getInventario().buscar("Flecha").getQuantidade());
        assertEquals(21, edgar.getExperiencia());
        assertEquals(Status.MORTO, edgar.getStatus());
        assertEquals(0, edgar.getVida(), 0.1);
    }
    
}
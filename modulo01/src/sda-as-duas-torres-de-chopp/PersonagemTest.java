import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class PersonagemTest{
    @Test
    public void personagemGanhaUmItem(){
        Item polenta = new Item("Polenta", 3);
        Personagem eustacio = new Elfo("Eustacio");
        assertNull(eustacio.getInventario().obter(2));
        eustacio.ganharItem(polenta);
        assertTrue(polenta.equals(eustacio.getInventario().obter(2)));
    }
    @Test
    public void personagemPerdeUmItem(){
        Personagem alfredo = new Elfo("Alfredo");
        assertTrue(new Item("Flecha", 7).equals(alfredo.getInventario().obter(1)));
        alfredo.perderItem(new Item("Flecha", 7));
        assertNull(alfredo.getInventario().obter(1));
    }
}

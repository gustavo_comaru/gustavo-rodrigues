package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.domain.User;
import br.com.dbc.locadora.rest.dto.UserDTO;
import br.com.dbc.locadora.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/user")
public class UserRestController {
    
    @Autowired
    private UserService userService;
    
    @PostMapping()
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> save(@RequestBody UserDTO input) {
        User result = userService.create(input);
        if(result == null)
            return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(result);
    }
    
    @PostMapping("/password")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> changePassword(@RequestBody UserDTO input) {
        User result = userService.changePassword(input);
        if(result != null)
            return ResponseEntity.ok(result);
        return ResponseEntity.badRequest().build();
    }
    
}
public class ClienteService extends AbstractCRUDService< Cliente, Long, ClienteDAO > {
    
    private static ClienteService instance;
    
    static{
        instance = new ClienteService();
    }
    
    public static ClienteService getInstance(){
        return instance;
    }
    
    private ClienteService(){}
    
    @Override
    protected ClienteDAO getDao() {
        return ClienteDAO.getInstance();
    }
   
}
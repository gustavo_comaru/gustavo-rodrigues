package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.enums.MidiaType;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MidiaRepository extends JpaRepository<Midia, Long> {
    
    public Long countByTipo( MidiaType tipo );
    public Long countByTipoAndIdFilme( MidiaType tipo, Long idFilme );
    public List<Midia> findByIdAluguel( Long idAluguel );
    public List<Midia> findByIdFilme( Long idFilme );
    public List<Midia> findByIdFilmeAndTipo( Long idFilme, MidiaType tipo );
    public Optional<Midia> findFirstByIdFilmeAndTipoAndIdAluguel( Long idFilme, MidiaType tipo, Long idAluguel);
    /*
    public Page<Midia> findByFilmeTituloIgnoreCaseOrFilmeCategoriaOrFilmeLancamentoBetween(
        String titulo,
        Categoria categoria,
        LocalDate ini,
        LocalDate fin,
        Pageable pageable
    );
    */
}

public class Numeros{

    private double array[];
    
    public Numeros(double array[]){
        this.array = array;
    }
    
    public double[] getArray(){
        return array;
    }
    
    public int getArrayLength(){
        return array.length;
    }

    public double[] calcularMediaSeguinte(){
        double result[];
        result = new double[array.length - 1];
        
        int pos = 0;
        while(pos < result.length){
            result[pos] = (array[pos] + array[pos+1]) / 2.0;
            pos++;
        }
        
        return result;
    }
}
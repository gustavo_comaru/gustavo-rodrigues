import java.util.*;

public class AtaqueIntercalado implements Estrategias{
    
    public List<Elfo> getOrdemDeAtaque(List<Elfo> soldados){
        List<Elfo> soldadosInstruidos = new ArrayList<>();
        
        
        
        for(int i=0; i < soldados.size(); i++){
            if(soldados.get(i).getStatus() == Status.VIVO){
                if(soldados.get(i).getClass() == ElfoVerde.class)
                    soldadosInstruidos.add(soldados.get(i));
            }
        }
        
        for(int i=0; i < soldados.size(); i++){
            if(soldados.get(i).getStatus() == Status.VIVO){
                if(soldados.get(i).getClass() == ElfoNoturno.class)
                    soldadosInstruidos.add(soldados.get(i));
            }
        }
        
        return soldadosInstruidos;
    }
    
}

package br.com.dbc.petshophibernate.entity;

public abstract class AbstractEntity<ID> {
    
    public abstract ID getId();
    
}

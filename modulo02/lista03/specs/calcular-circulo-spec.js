describe( "calcular-circulo", function(){

  beforeEach( function(){
    chai.should()
  } )

  it( "deve calcular area de raio 1", function(){
    const raio = 1, tipoCalculo = 'A'
    const resultado = calcularCirculo( { raio, tipoCalculo } )
    resultado.should.equal( Math.PI )
  } )

  it( "deve calcular circunferência de raio 1", function(){
    const raio = 1, tipoCalculo = 'C'
    const resultado = calcularCirculo( {raio, tipoCalculo} )
    resultado.should.equal( 2 * Math.PI )
  } )

  it( "deve calcular area de raio 2.5", function(){
    const raio = 2.5, tipoCalculo = 'A'
    const resultado = calcularCirculo( {raio, tipoCalculo} )
    resultado.should.equal( 6.25 * Math.PI ) 
  } )

  it( "deve calcular circunferencia de raio 2.5", function(){
    const raio = 2.5, tipoCalculo = 'C'
    const resultado = calcularCirculo( {raio, tipoCalculo} )
    resultado.should.equal( 5 * Math.PI )
  } )

  it( "deve calcular uma string", function(){
    const tipoResultado = typeof calcularCirculo("abacate")
    tipoResultado.should.equal( "undefined" )
  } )

  it("recebe apenas o raio", function() {
    const raio = 10
    const tipoResultado = typeof calcularCirculo( {raio} )
    tipoResultado.should.equal( "undefined" )
  } )

} )

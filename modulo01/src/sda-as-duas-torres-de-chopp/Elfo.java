public class Elfo extends Personagem{
    
    public static int qtdTotalDeElfos = 0;
    
    protected int experiencia;    
    protected int MOD_EXP;
    
    {
        experiencia = 0;
        MOD_EXP = 1;
    }
    
    public Elfo(String nome){
        super(nome, 100.0, Status.VIVO);
        super.ganharItem(new Item("Arco", 1));
        super.ganharItem(new Item("Flecha", 7));
        qtdTotalDeElfos++;
    }
    
    @Override
    protected void finalize() throws Throwable{
        qtdTotalDeElfos--;
    }
    
    public int getExperiencia(){
        return this.experiencia;
    }
    
    public void atirarFlecha(Dwarf alvo){
        Item flecha = inventario.buscar("Flecha");
        if(status == Status.MORTO || flecha == null || flecha.getQuantidade() <= 0) return;
        flecha.setQuantidade(flecha.getQuantidade() - 1);
        experiencia += MOD_EXP;
        alvo.sofrerDano();
        sofrerDano();
    }
    
}
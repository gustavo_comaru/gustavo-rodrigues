package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.enums.Categoria;
import br.com.dbc.locadora.entity.Filme;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmeRepository extends JpaRepository<Filme, Long> {
 
    public Page<Filme> findByTituloContainingIgnoreCaseOrCategoriaOrLancamentoBetween(
        String titulo,
        Categoria categoria,
        LocalDate ini,
        LocalDate fin,
        Pageable pageable
    );
    
    public List<Filme> findByTituloContainingIgnoreCaseOrCategoriaOrLancamentoBetween(
        String titulo,
        Categoria categoria,
        LocalDate ini,
        LocalDate fin
    );
    
    public List<Filme> findByTituloContainingIgnoreCaseOrCategoriaOrLancamento(
        String titulo,
        Categoria categoria,
        LocalDate lancamento
    );
    
}

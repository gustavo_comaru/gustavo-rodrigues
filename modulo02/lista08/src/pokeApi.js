class PokeApi{

  constructor(url){
    this.url = url
  }

  async buscar(idPokemon) {

    let urlPokemon = `${ this.url }/${ idPokemon }/`
    
    return new Promise( resolve => {

      fetch(urlPokemon)
        .then( stringPokemon => stringPokemon.json() )
        .then( objetoPokemonApi => {
          resolve( new Pokemon(objetoPokemonApi) )
        } )
    
      } )
  
  }

}

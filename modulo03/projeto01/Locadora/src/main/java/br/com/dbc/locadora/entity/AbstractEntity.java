package br.com.dbc.locadora.entity;

public abstract class AbstractEntity<ID extends Number> {
    public abstract ID getId();
}
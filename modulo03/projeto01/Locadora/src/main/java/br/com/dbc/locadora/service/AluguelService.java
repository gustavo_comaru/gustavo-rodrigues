package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.enums.MidiaType;
import br.com.dbc.locadora.repository.AluguelRepository;
import br.com.dbc.locadora.rest.dto.AluguelInDTO;
import br.com.dbc.locadora.rest.dto.AluguelOutDTO;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AluguelService extends AbstractCrudService<Aluguel, Long, AluguelRepository>{
        
    @Autowired
    private AluguelRepository aluguelRepository;
    
    @Autowired
    private MidiaService midiaService;

    @Autowired
    private FilmeService filmeService;
    
    @Autowired
    private ClienteService clienteService;
    
    @Override
    protected AluguelRepository getRepository(){
        return aluguelRepository;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public AluguelOutDTO retirar( AluguelInDTO dto ){
        
        if( clienteService.findById( dto.getIdCliente() ).get() == null )
            return null;
        
        boolean algumaMidiaIndisponivel = false;
        for( Long idMidia : dto.getMidias() ){
            if( midiaService.taAlugada( idMidia ) )
                algumaMidiaIndisponivel = true;
        }
        if(algumaMidiaIndisponivel)
            return null;
        
        LocalDate dataHoje = LocalDate.now();
        
        int prazoDevolucao = dto.getMidias().size();
        
        Double valorAluguel = 0.0;
        for( Long idMidia : dto.getMidias() ){
            valorAluguel += midiaService.findValorAtual( idMidia );
        }
        
        Aluguel novoAluguel = Aluguel.builder()
            .retirada(dataHoje)
            .previsao(dataHoje.plusDays(prazoDevolucao))
            .devolucao(null)
            .multa(null)
            .idCliente(dto.getIdCliente())
            .build();
        
        Aluguel aluguelSalvo = getRepository().save(novoAluguel);
        
        midiaService.retirarMidias( dto.getMidias(), aluguelSalvo.getId() );
        
        return AluguelOutDTO.builder()
            .id( aluguelSalvo.getId() )
            .retirada( aluguelSalvo.getRetirada() )
            .previsao( aluguelSalvo.getPrevisao() )
            .devolucao( aluguelSalvo.getDevolucao() )
            .valor( valorAluguel )
            .multa( aluguelSalvo.getMulta() )
            .idCliente( aluguelSalvo.getIdCliente() )
            .build();
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Aluguel devolver( AluguelInDTO dto ){
    
        Aluguel aluguel = aluguelRepository.findById( 
            midiaService.findById( dto.getMidias().get(0) ).get().getIdAluguel()
        ).get();
        
        List<Midia> midiasDoAluguel = midiaService.findByIdAluguel( aluguel.getId() );
        List<Long> idsMidiasDoAluguel = new ArrayList<Long>();
        for( Midia m : midiasDoAluguel ){
            idsMidiasDoAluguel.add( m.getId() );
        }
        
        if( ! dto.getMidias().containsAll( idsMidiasDoAluguel ) )
            return null;
        
        
        LocalDateTime agora = LocalDateTime.now();
        
        LocalDateTime previsao = aluguel.getPrevisao().atStartOfDay().plusHours(16);
        
        boolean deveMultar = agora.isAfter(previsao);
        
        Double valorAluguel = 0.0;
        for( Long idMidia : dto.getMidias() ){
            valorAluguel += midiaService.findValorVigenteNoPeriodo( idMidia, aluguel.getRetirada().atStartOfDay() );
        }

        Double multa = 0.0;

        if( deveMultar )
            multa = valorAluguel;

        
        aluguel.setDevolucao(agora);
        aluguel.setMulta(multa);
        
        save(aluguel);
        
        midiaService.devolverMidias(dto.getMidias());
        
        return aluguel;
        
    }
    
    
    public List<Filme> previsaoHoje(){
        
        List<Aluguel> alugueisHoje = aluguelRepository.findByPrevisao( LocalDate.now() );
        
        List<Filme> filmesHoje = new ArrayList<Filme>();
        List<Midia> midiasDoAluguel;
        for( Aluguel aluguel : alugueisHoje ){
            midiasDoAluguel = midiaService.findByIdAluguel(aluguel.getId());
            for( Midia midia : midiasDoAluguel ){
                filmesHoje.add( filmeService.findById( midia.getIdFilme() ).get() );
            }
        }
        
        return filmesHoje;
        
    }
    
    public LocalDateTime acharProximaPrevisao( Long idFilme, MidiaType tipo ){
        List<Midia> todasMidiasDoFilme = midiaService.acharTodasMidiasDesseFilmeETipo(idFilme, tipo);
        LocalDate maisRecente = aluguelRepository.findById(todasMidiasDoFilme.get(0).getIdAluguel()).get().getPrevisao();
        for(Midia midia : todasMidiasDoFilme){
            LocalDate dataEmQuestao = aluguelRepository.findById(midia.getIdAluguel()).get().getPrevisao();
            if(dataEmQuestao.isBefore(maisRecente))
                maisRecente = dataEmQuestao;
        }
        return maisRecente.atStartOfDay().plusHours(16);
    }
    
}

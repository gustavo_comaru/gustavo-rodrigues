import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class DwarfBarbaLongaTest{
    @Test
    public void sofreDano(){
        Dwarf barbudo = new DwarfBarbaLonga("Barbudo", new DadoFake(4));
        barbudo.sofrerDano();
        assertEquals(100.0, barbudo.getVida(), 0.1);
    }
}

public class Numero{

    private int numero;
    
    public Numero(int num){
        numero = num;
    }
    
    public boolean impar(){
        
        return numero % 2 == 1;
        
        //return (numero % 2 == 1) ? true : false;
        
        //if(numero%2 == 0)
        //   return false;
        //return true;
    
    }
    
    public boolean verificarSomaDivisivel(int divisor){
        
        if(divisor == 0) return true;
        
        int soma = 0;
        while(divisor != 0){
            soma += divisor % 10;
            divisor /= 10;
        }
        
        return (soma % numero == 0);
    }
    
}
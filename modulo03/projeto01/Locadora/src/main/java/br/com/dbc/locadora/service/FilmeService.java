package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.enums.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.rest.dto.FilmeCatalogoDTO;
import br.com.dbc.locadora.rest.dto.FilmeDTO;
import br.com.dbc.locadora.rest.dto.MidiaOutDTO;
import br.com.dbc.locadora.rest.dto.ValorMidiaDTO;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FilmeService extends AbstractCrudService<Filme, Long, FilmeRepository>{
        
    @Autowired
    private FilmeRepository filmeRepository;
    
    @Autowired
    private MidiaService midiaService;
    
    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected FilmeRepository getRepository(){
        return filmeRepository;
    }
    
    public boolean categoriaEhValida( String supostaCategoria ){
        String[] categorias = { "ACAO", "AVENTURA", "ANIMACAO" };
        if(! Arrays.asList(categorias).contains(supostaCategoria) )
            return false;
        return true;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme saveFromDTO( FilmeDTO dto ){
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        
        LocalDate novaData = null;
        if(dto.getLancamento() != null)
            novaData = LocalDate.parse(dto.getLancamento(), dtf);
        
        Categoria novaCategoria = null;
        if(dto.getCategoria() != null)
            novaCategoria = Categoria.valueOf(dto.getCategoria());
        
        Filme novoFilme = Filme.builder()
                .titulo(dto.getTitulo())
                .lancamento(novaData)
                .categoria(novaCategoria)
                .build();
        
        Filme filmeSalvo = getRepository().save(novoFilme);
        
        midiaService.saveFromDTOList( dto.getMidia(), filmeSalvo.getId() );
        
        return filmeSalvo;
    
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme updateFromDTO( Long idFilme, FilmeDTO dto ){
        
        Filme filme = filmeRepository.findById(idFilme).get();
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        
        LocalDate novaData = null;
        if(dto.getLancamento() != null)
            novaData = LocalDate.parse( dto.getLancamento(), dtf );
        
        Categoria novaCategoria = null;
        if(dto.getCategoria() != null)
            novaCategoria = Categoria.valueOf( dto.getCategoria() );
        
        filme.setTitulo( dto.getTitulo() );
        filme.setLancamento( novaData );
        filme.setCategoria( novaCategoria );
        
        filmeRepository.save(filme);
        
        midiaService.updateFromDTOList( dto.getMidia(), filme.getId() );
                
        return filme;
    
    }
    
    public List<ValorMidiaDTO> getHistoricoValores( Long idFilme ){
    
        List<Midia> midias = midiaService.findByIdFilme( idFilme );
        
        List<ValorMidia> valores = new ArrayList<ValorMidia>();
        for( Midia midia : midias ){
            valores.addAll( valorMidiaService.findByIdMidia( midia.getId() ) );
        }
        
        List<ValorMidiaDTO> resultado = new ArrayList<ValorMidiaDTO>();
        for( ValorMidia val : valores ){
            resultado.add( ValorMidiaDTO.builder()
                .id( val.getId() )
                .tipo( midiaService.findById( val.getIdMidia() ).get().getTipo() )
                .valor( val.getValor() )
                .inicioVigencia( val.getInicioVigencia() )
                .finalVigencia( val.getFinalVigencia() )
                .build()
            );
        }
        
        return resultado;
    }
    
    public Page<Filme> search( String titulo, Categoria categoria, LocalDate ini, LocalDate fin, Pageable pageable ){
        if(ini == null)
            ini = LocalDate.MIN;
        if(fin == null)
            fin = LocalDate.MIN;
        return filmeRepository.findByTituloContainingIgnoreCaseOrCategoriaOrLancamentoBetween(titulo,categoria,ini,fin,pageable);
    
    }
    
        
    public Page<MidiaOutDTO> searchMidia( String titulo, Categoria categoria, LocalDate ini, LocalDate fin, Pageable pageable ){
        
        if(ini == null)
            ini = LocalDate.MIN;
        if(fin == null)
            fin = LocalDate.MIN;
        
        List<Filme> filmes = filmeRepository.findByTituloContainingIgnoreCaseOrCategoriaOrLancamentoBetween(titulo,categoria,ini,fin,pageable).getContent();
        List<MidiaOutDTO> listaResultado = new ArrayList<MidiaOutDTO>();
        
        for( Filme filme : filmes ){
          
            for( Midia midia : midiaService.findByIdFilme( filme.getId() ) ){
                listaResultado.add( MidiaOutDTO.builder()
                    .id( midia.getId() )
                    .tipo( midia.getTipo() )
                    .valor( midiaService.findValorAtual( midia.getId() ) )
                    .filme( filme )
                    .build()
                );
            }
            
        }
        
        return new PageImpl<>( listaResultado, pageable, listaResultado.size() );
                
    }
    
    public Page<FilmeCatalogoDTO> searchCatalogo(FilmeDTO dto, Pageable pageable){
        
        List<Filme> filmes;

        if( dto.getTitulo() != null || dto.getCategoria() != null || dto.getLancamento() != null ){
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate data = null;
            if(dto.getLancamento() != null)
                data = LocalDate.parse( dto.getLancamento(), dtf );
            Categoria categoria = null;
            if(dto.getCategoria() != null)
                categoria = Categoria.valueOf( dto.getCategoria() );
            filmes = filmeRepository.findByTituloContainingIgnoreCaseOrCategoriaOrLancamento(dto.getTitulo(), categoria, data);
        }
        else
            filmes = filmeRepository.findAll();
            
        List<FilmeCatalogoDTO> resultado = new ArrayList<FilmeCatalogoDTO>();
        for(Filme filme : filmes){
            resultado.add(
                FilmeCatalogoDTO.builder()
                    .titulo(filme.getTitulo())
                    .categoria(filme.getCategoria())
                    .dataDeLancamento(filme.getLancamento())
                    .midiasDisponiveis(midiaService.catalogarPorFilme(filme.getId()))
                    .build()
            );
        }
        return new PageImpl<>( resultado, pageable, resultado.size() );
    }
    
    public Page<FilmeCatalogoDTO> searchCatalogoAll(Pageable pageable){
        List<FilmeCatalogoDTO> resultado = new ArrayList<FilmeCatalogoDTO>();
        for(Filme filme : filmeRepository.findAll()){
            resultado.add(
                FilmeCatalogoDTO.builder()
                    .titulo(filme.getTitulo())
                    .categoria(filme.getCategoria())
                    .dataDeLancamento(filme.getLancamento())
                    .midiasDisponiveis(midiaService.catalogarPorFilme(filme.getId()))
                    .build()
            );
        }
        return new PageImpl<>( resultado, pageable, resultado.size() );
    }
        
    
}
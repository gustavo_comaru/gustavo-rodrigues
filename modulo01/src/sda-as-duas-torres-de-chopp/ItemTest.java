import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemTest{
    @Test
    public void itemCriadoComDescricaoCerta(){
        assertEquals("Batata", new Item("Batata", 20).getDescricao());
    }
    @Test
    public void itemCriadoComQuantidadeCerta(){
        assertEquals(20, new Item("Batata", 20).getQuantidade());
    }
    @Test
    public void modificarQuantidade(){
        Item abajur = new Item("Abajur", 3);
        abajur.setQuantidade(5);
        assertEquals(5, abajur.getQuantidade());
    }
    @Test
    public void modificarQuantidadeParaNegativa(){
        Item abacaxi = new Item("Abacaxi", 2);
        abacaxi.setQuantidade(-3);
        assertEquals(-3, abacaxi.getQuantidade());
    }
    @Test
    public void comprarItensComReferenciasDiferentesComMetodoEqual(){
        Item espada = new Item("Espada", 2);
        boolean saoIguais = espada.equals(new Item("Espada", 2));
        assertTrue(saoIguais);
    }
}
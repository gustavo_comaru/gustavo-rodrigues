import java.util.ArrayList;

public class Inventario{
    
    private ArrayList<Item> itens;
    
    public Inventario(){
        this.itens = new ArrayList<Item>();
    }
    
    public ArrayList<Item> getItens(){
        return this.itens;
    }
       
    public int size(){
        return itens.size();
    }
    
    public void adicionar(Item itemAdicionado){
        this.itens.add(itemAdicionado);
    }
    
    public Item obter(int posicao){
        if(posicao >= this.itens.size()) return null;
        return this.itens.get(posicao);
    }
    
    public void remover(int posicao){
        if(posicao >= this.itens.size()) return;
        this.itens.remove(posicao);
    }
    
    public void remover(Item item){
        int i = 0;
        while(!itens.get(i).equals(item)){
            if(i == itens.size()) return;
            i++;
        }
        itens.remove(i);
    }
    
    public String getDescricoes(){
        if(itens.isEmpty()) return "";
        StringBuilder result = new StringBuilder();
        int pos = 0;
        boolean devePorVirgula = false;
        while(pos < itens.size()){
            if(itens.get(pos) != null){
                if(devePorVirgula) result.append(",");
                result.append(itens.get(pos).getDescricao());
                devePorVirgula = true;
            }
            pos++;
        }
        return result.toString();
    }
 
    public Item procurarMaiorQtd(){
        if(itens.isEmpty()) return null;
        int maiorPos = 0;
        int maiorQtd = itens.get(0).getQuantidade();
        int pos = 1;
        while(pos < itens.size()){
            if(itens.get(pos) != null && itens.get(pos).getQuantidade() > maiorQtd){
                maiorQtd = itens.get(pos).getQuantidade();
                maiorPos = pos;
            }
            pos++;
        }
        return itens.get(maiorPos);
    }
    
    public Item buscar(String descricaoProcurada){
        for(int i = 0; i < itens.size(); i++){
            if(itens.get(i)!=null && itens.get(i).getDescricao().equals(descricaoProcurada)){
                return itens.get(i);
            }
        }
        return null;
    }
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> itensInvertidos = new ArrayList<Item>();
        for(int i = itens.size() - 1; i >= 0; i--){
            if(itens.get(i) != null){
                itensInvertidos.add(itens.get(i));
            }
        }
        return itensInvertidos;
    }
    
    public int quantidadeDeItens(){
        int quantidade = 0;
        for(int i = 0; i < itens.size(); i++){
            if(itens.get(i) != null){
                quantidade++;
            }
        }
        return quantidade;
    }
    
    public void ordenarItens(){
        ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens(TipoOrdenacao ordenacao){
        if(itens.isEmpty()){
            return;
        }
        ArrayList<Item> itensEmOrdem = new ArrayList<Item>();
        for(int j = quantidadeDeItens(); j > 0; j--){
            int indice = 0;        
            int quantidade = itens.get(0).getQuantidade();
            for(int i = 1; i < itens.size(); i++){
                if(itens.get(i) != null){
                    if(ordenacao == TipoOrdenacao.ASC && itens.get(i).getQuantidade() < quantidade || ordenacao == TipoOrdenacao.DESC && itens.get(i).getQuantidade() > quantidade){
                        indice = i;
                        quantidade = itens.get(i).getQuantidade();
                    }
                }
            }
            itensEmOrdem.add(itens.get(indice));
            itens.remove(indice);
        }
        itens = itensEmOrdem;
    }
    
}
package br.com.dbc.minhafloricultura.ws;

import br.com.dbc.minhafloricultura.dao.ClienteDAO;
import br.com.dbc.minhafloricultura.entity.Cliente;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

@Stateless
@WebService(serviceName = "ClienteWS")
public class ClienteWS extends AbstractCrudWS<ClienteDAO, Cliente>{

    @EJB
    private ClienteDAO clienteDAO;

    @Override
    @WebMethod(exclude = true)
    public ClienteDAO getDAO(){
        return clienteDAO;
    }
    
}

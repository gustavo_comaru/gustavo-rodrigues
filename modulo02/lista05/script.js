const meuH2 = document.getElementById("titulo")
const meuButton = document.getElementById("botao")

testarBotao()

function perguntarNome(){
  const nome = prompt("Qual seu nome?")
  meuH2.innerText = nome
  localStorage.nome = nome
}

function renderizaNomeArmazenadoNaTela(){
  meuH2.innerText = localStorage.nome
}

if (localStorage.nome && localStorage.nome.length > 0)
  renderizaNomeArmazenadoNaTela()
else
  perguntarNome()

meuButton.onclick = function() {
  localStorage.removeItem("nome")
  if(! meuButton.disabled)
    localStorage.contadorCliques = typeof localStorage.contadorCliques === "undefined" ? 1 : 1 * localStorage.contadorCliques + 1
    testarBotao()
}

function testarBotao(){
  if(1 * localStorage.contadorCliques >= 5)
    meuButton.disabled = true
}

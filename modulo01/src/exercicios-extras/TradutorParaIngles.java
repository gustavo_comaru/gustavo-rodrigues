public class TradutorParaIngles implements Tradutor{
    public String traduzir(String textoPT){
        switch(textoPT){
            case       "oi" : return "hi";
            case "obrigado" : return "thank you";
            case      "sim" : return "yes";
            default         : return "";
        }
    }
}
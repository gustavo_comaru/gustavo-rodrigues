public class TradutorParaAlemao implements Tradutor{
    public String traduzir(String textoPT){
        switch(textoPT){
            case       "oi" : return "hallo";
            case "obrigado" : return "danke";
            case      "sim" : return "ja";
            default         : return "";
        }
    }
}
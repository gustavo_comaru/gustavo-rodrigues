import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemImperdivelTest{
    @Test
    public void itemImperdivelNaoPodeSerConstruidoComQuantidadeZero(){
        Item item = new ItemImperdivel("Item", 0);
        assertEquals(1, item.getQuantidade());
    }
    @Test
    public void itemImperdivelNaoPodeSerConstruidoComQuantidadeNegatica(){
        Item item = new ItemImperdivel("Item", -10);
        assertEquals(1, item.getQuantidade());
    }
    @Test
    public void itemImperdivelNaoPodeSerZerado(){
        Item anduril = new ItemImperdivel("Anduril", 1);
        anduril.setQuantidade(0);
        assertEquals(1, anduril.getQuantidade());
    }
    @Test
    public void itemImperdivelNaoPodeSerNegativo(){
        Item escudo = new ItemImperdivel("Escudo de Madeira", 1);
        escudo.setQuantidade(-2);
        assertEquals(1, escudo.getQuantidade());
    }
    @Test
    public void itemImperdivelPodeReceberOutraQuantidade(){
        Item salgado = new ItemImperdivel("Salgado", 1);
        salgado.setQuantidade(99);
        assertEquals(99, salgado.getQuantidade());
    }
}
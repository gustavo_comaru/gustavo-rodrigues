import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class InventarioTest{
    @Test
    public void adicionarPrimeiroItem(){
        Inventario inventario = new Inventario();
        Item dicionario = new Item("Dicionario Aurelio", 2);
        inventario.adicionar(dicionario);
        assertEquals(dicionario, inventario.obter(0));
    }
    @Test
    public void obterItemDePosicaoInexistente(){
        Inventario inventario = new Inventario();
        assertNull(inventario.obter(1000));        
    }
    @Test
    public void removerUmItem(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Batata", 1));
        inventario.remover(0);
        assertNull(inventario.obter(0));
    }
    @Test
    public void addDepoisDeRemoverItemPosteriorAoAPreencher(){
        Item a = new Item("Abacate", 1);
        Item b = new Item("Beringela", 2);
        Item c = new Item("Cenoura", 3);
        Inventario inventario = new Inventario();
        inventario.adicionar(a);
        inventario.adicionar(b);
        inventario.adicionar(c);
        inventario.remover(0);
        inventario.remover(1);
        inventario.adicionar(b);
        inventario.adicionar(a);
        ArrayList<Item> esperado = new ArrayList<Item>();
        esperado.add(b);
        esperado.add(b);
        esperado.add(a);
        assertEquals(esperado, inventario.getItens());
    }
    @Test
    public void testarGetDescricoes(){
        Item a = new Item("Abacate", 1);
        Item b = new Item("Beringela", 2);
        Item c = new Item("Cenoura", 3);
        Inventario inventario = new Inventario();
        inventario.adicionar(a);
        inventario.adicionar(b);
        inventario.adicionar(c);
        inventario.adicionar(b);
        inventario.adicionar(a);
        inventario.remover(0);
        inventario.remover(2);
        assertEquals("Beringela,Cenoura,Abacate", inventario.getDescricoes());
    }
    @Test
    public void testarProcurarMaiorPosicao(){
        Item a = new Item("Grao de Arroz", 1030);
        Item b = new Item("Bergamota", 4);
        Item c = new Item("Carne de Coelho", 2);
        Inventario inventario = new Inventario();
        inventario.adicionar(b);
        inventario.adicionar(b);
        inventario.adicionar(c);
        inventario.adicionar(c);
        inventario.adicionar(b);
        inventario.adicionar(a);
        inventario.remover(0);
        inventario.remover(3);
        assertEquals(a, inventario.procurarMaiorQtd());
    }
    @Test
    public void procurarDescricaoEmSituacaoPadrao(){
        Item a = new Item("Abacate", 1);
        Item b = new Item("Beringela", 2);
        Item c = new Item("Cenoura", 3);
        Inventario inventario = new Inventario();
        inventario.adicionar(a);
        inventario.adicionar(b);
        inventario.adicionar(c);
        assertEquals(c, inventario.buscar("Cenoura"));
    }
    @Test
    public void procurarPorDescricaoDeItensRepetidos(){ //encontrar primeira ocorrencia
        Item a = new Item("Abacate", 10);
        Item b = new Item("Abacate", 20);
        Inventario inventario = new Inventario();
        inventario.adicionar(a);
        inventario.adicionar(b);
        assertEquals(a, inventario.buscar("Abacate"));
    }
    @Test
    public void procurarPorDescricaoItemInexistente(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Nao Abacate", 1));
        assertNull(inventario.buscar("Abacate"));
    }
    @Test
    public void procurarPorDescricaoEmInventarioVazio(){
        assertNull(new Inventario().buscar("Abacate"));
    }
    @Test
    public void inverterListaDeItens(){
        Item a = new Item("Abacate", 1);
        Item b = new Item("Beringela", 2);
        Item c = new Item("Cenoura", 3);
        Inventario inventario = new Inventario();
        inventario.adicionar(a);
        inventario.adicionar(b);
        inventario.adicionar(c);
        ArrayList<Item> listaCorreta = new ArrayList<Item>();
        listaCorreta.add(c);
        listaCorreta.add(b);
        listaCorreta.add(a);
        assertEquals(listaCorreta, inventario.inverter());
    }
    @Test
    public void ordenarAscInventarioVazio(){
        Inventario inventario1 = new Inventario();
        Inventario inventario2 = new Inventario();
        inventario1.ordenarItens(TipoOrdenacao.ASC);
        assertEquals(inventario2.getItens(), inventario1.getItens());
    }
    @Test
    public void ordenarAscInventarioComItens(){
        Item a = new Item("Abacate", 1);
        Item b = new Item("Beringela", 2);
        Item c = new Item("Cenoura", 3);
        Inventario inventario1 = new Inventario();
        Inventario inventario2 = new Inventario();    
        inventario1.adicionar(c);
        inventario1.adicionar(b);
        inventario1.adicionar(a);
        inventario1.adicionar(c);
        inventario1.adicionar(b);
        inventario2.adicionar(a);
        inventario2.adicionar(b);
        inventario2.adicionar(b);
        inventario2.adicionar(c);
        inventario2.adicionar(c);
        inventario1.ordenarItens(TipoOrdenacao.ASC);
        assertEquals(inventario2.getItens(), inventario1.getItens());        
    }
    @Test
    public void ordenarDescInventarioVazio(){
        Inventario inventario1 = new Inventario();
        Inventario inventario2 = new Inventario();
        inventario1.ordenarItens(TipoOrdenacao.DESC);
        assertEquals(inventario2.getItens(), inventario1.getItens());
    }
    @Test
    public void ordenarDescInventarioComItens(){
        Item a = new Item("Abacate", 1);
        Item b = new Item("Beringela", 2);
        Item c = new Item("Cenoura", 3);
        Inventario inventario1 = new Inventario();
        Inventario inventario2 = new Inventario();    
        inventario1.adicionar(c);
        inventario1.adicionar(b);
        inventario1.adicionar(a);
        inventario1.adicionar(c);
        inventario1.adicionar(b);
        inventario2.adicionar(c);
        inventario2.adicionar(c);
        inventario2.adicionar(b);
        inventario2.adicionar(b);
        inventario2.adicionar(a);
        inventario1.ordenarItens(TipoOrdenacao.DESC);
        assertEquals(inventario2.getItens(), inventario1.getItens());      
    }
}
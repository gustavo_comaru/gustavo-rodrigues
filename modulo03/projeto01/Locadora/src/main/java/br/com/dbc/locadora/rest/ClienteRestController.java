package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/cliente")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class ClienteRestController extends AbstractCrudRestController<Cliente, Long, ClienteService> {
    
    @Autowired
    private ClienteService clienteService;

    @Override
    protected ClienteService getService() {
        return clienteService;
    }
    
    @GetMapping("/cep/{cep}")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity consultarCep(@PathVariable String cep){
        return ResponseEntity.ok(clienteService.consultarCep(cep));
    }

}
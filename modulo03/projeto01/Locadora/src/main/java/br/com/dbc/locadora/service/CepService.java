package br.com.dbc.locadora.service;

import br.com.dbc.locadora.rest.dto.EnderecoDTO;
import br.com.dbc.locadora.ws.ConsultaCEP;
import br.com.dbc.locadora.ws.ConsultaCEPResponse;
import br.com.dbc.locadora.ws.EnderecoERP;
import br.com.dbc.locadora.ws.ObjectFactory;
import javax.xml.bind.JAXBElement;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class CepService extends WebServiceGatewaySupport {
 
    private String urlCorreiosAtendeCliente = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente";
    
    public Object callWebService(String url, Object request){
        return getWebServiceTemplate().marshalSendAndReceive(url, request);
    }
    
    public Object consultarCepPorObjetoConsulta(ConsultaCEP consulta){
        return callWebService(urlCorreiosAtendeCliente, new ObjectFactory().createConsultaCEP(consulta));
    }
    
    public Object consultarCepPorString(String cep){
        ConsultaCEP consulta = new ConsultaCEP();
        consulta.setCep(cep);
        return consultarCepPorObjetoConsulta(consulta);
    }
    
    public EnderecoDTO formatarCep(Object obj){
        EnderecoERP endERP = ((ConsultaCEPResponse) ((JAXBElement) obj).getValue()).getReturn();
        return EnderecoDTO.builder()
            .rua(endERP.getEnd())
            .bairro(endERP.getBairro())
            .cidade(endERP.getCidade())
            .estado(endERP.getUf())
            .build();
    }
    
    public EnderecoDTO consultarUltimate(String cep){
        return formatarCep( consultarCepPorString(cep) );
    }
}

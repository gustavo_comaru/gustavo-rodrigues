( function main(){

  const h1 = document.getElementById("countdown")

  var numeroContador = 1 + 30

  var contador = setInterval( function(){
    contadorVaiPraZero() ? pararContador() : decrementarContador()
  }, 1000)

  var contadorVaiPraZero = function(){
    return numeroContador === 1 ? true : false
  }

  var decrementarContador = function(){
    numeroContador--
    h1.innerText = numeroContador
  }

  var pararContador = function(){
    clearInterval(contador)
    h1.innerText = "Tempo encerrado."
  }

} ) ()

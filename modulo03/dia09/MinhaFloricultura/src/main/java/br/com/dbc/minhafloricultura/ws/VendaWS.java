package br.com.dbc.minhafloricultura.ws;

import br.com.dbc.minhafloricultura.dao.VendaDAO;
import br.com.dbc.minhafloricultura.entity.Venda;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

@Stateless
@WebService(serviceName = "VendaWS")
public class VendaWS extends AbstractCrudWS<VendaDAO, Venda>{

    @EJB
    private VendaDAO vendaDAO;

    @Override
    @WebMethod(exclude = true)
    public VendaDAO getDAO(){
        return vendaDAO;
    }
    
}

import Pokemon from '../models/pokemon.js'

export default class PokeApi{

  constructor(url){
    this.url = url
  }

  async buscarPorUrl(urlPokemon){
    return new Promise( resolve => {
    fetch(urlPokemon)
      .then( stringPokemon => stringPokemon.json() )
      .then( objetoPokemonApi => {
        resolve( new Pokemon(objetoPokemonApi) )
      } )
    } )
  }

  async buscarPorId(idPokemon){
    let urlPokemon = `${ this.url }/${ idPokemon }/`
    return this.buscarPorUrl(urlPokemon)
  }

  async listarPorTipo(idTipo, qtsCarregar, pag){
    let urlTipo = `https://pokeapi.co/api/v2/type/${ idTipo }/`
    return new Promise( resolve => {
      fetch(urlTipo)
        .then( stringPokemons => stringPokemons.json() )
        .then( arrayPokemons => {
          let tamanho = arrayPokemons.pokemon.length
          let pokemons = arrayPokemons.pokemon.slice(qtsCarregar * ( pag - 1 ), qtsCarregar * pag )
          let promisesPkm = pokemons.map( p => this.buscarPorUrl( p.pokemon.url ) )
          Promise.all( promisesPkm ).then( paginaPorTipo => {
            const resultado = []
            resultado[0] = ( paginaPorTipo )         // a pagina em si
            resultado[1] = ( tamanho )               // o numero total de pokemons do tipo
            resolve( resultado )
          } )
        } )
    } )
  }
  
}

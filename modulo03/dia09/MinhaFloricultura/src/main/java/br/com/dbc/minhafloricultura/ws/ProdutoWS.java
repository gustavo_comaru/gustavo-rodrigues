package br.com.dbc.minhafloricultura.ws;

import br.com.dbc.minhafloricultura.dao.ProdutoDAO;
import br.com.dbc.minhafloricultura.entity.Produto;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

@Stateless
@WebService(serviceName = "ProdutoWS")
public class ProdutoWS extends AbstractCrudWS<ProdutoDAO, Produto>{

    @EJB
    private ProdutoDAO produtoDAO;

    @Override
    @WebMethod(exclude = true)
    public ProdutoDAO getDAO(){
        return produtoDAO;
    }
    
    public Produto findByDescription(String descricao){
        List<Produto> lista = produtoDAO.findAll();
        //to do:
        //filtrar lista por %descricao%
        //mudar pra retornar lista
        return new Produto();
    }
    
}

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NumerosTest{
    @Test
    public void testarMediaSeguinte1(){
        Numeros teste = new Numeros(new double[]{1,3,5,1,-10});
        assertArrayEquals(new double[]{2,4,3,-4.5}, teste.calcularMediaSeguinte(), 0.1);
    }
    @Test
    public void testarMediaSeguinte2(){
        Numeros teste = new Numeros(new double[]{0.002,1300,-3,0.0009,0.006});
        assertArrayEquals(new double[]{650.001,648.5,-1.49955,0.00345}, teste.calcularMediaSeguinte(), 0.1);
    }
}

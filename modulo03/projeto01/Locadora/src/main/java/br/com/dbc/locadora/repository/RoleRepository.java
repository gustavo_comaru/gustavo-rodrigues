package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
